package com.replay.timeclock;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeConverter {

    public static String stringFormat;
    public static SimpleDateFormat simpleDateFormat;

    public TimeConverter() {
        stringFormat = "MM/dd/yyyy hh:mm:ss aa";
        simpleDateFormat = new SimpleDateFormat(stringFormat);
    }

    public String dateToString(Calendar calendar){
        Date date = calendar.getTime();
        return simpleDateFormat.format(date);
    }

    public Calendar stringToDate(String string) {
        try {
            Date date = simpleDateFormat.parse(string);
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            return c;
        } catch (ParseException e) {

        }
        return null;
    }

    public float differenceInDate(Calendar a, Calendar b) {
        Date dateA = a.getTime();
        Date dateB = b.getTime();
        long diff = Math.abs(dateA.getTime() - dateB.getTime()); //Ans in milliseconds
        return diff / (1000*60*60); //return in hours
    }
}
