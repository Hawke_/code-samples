package com.replay.timeclock.listview;

import android.view.LayoutInflater;
import android.view.View;

/**
 * Created by Nathan on 1/1/2017.
 */

public interface Item {
    int getViewType();
    View getView(LayoutInflater inflater, View convertView);
    int getID();
}
