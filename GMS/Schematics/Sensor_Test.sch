<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="8.5.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="senior_project">
<packages>
<package name="ATTINY167">
<smd name="P$1" x="-5.08" y="5.08" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$2" x="-5.08" y="3.81" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$3" x="-5.08" y="2.54" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$4" x="-5.08" y="1.27" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$5" x="-5.08" y="0" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$6" x="-5.08" y="-1.27" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$7" x="-5.08" y="-2.54" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$8" x="-5.08" y="-3.81" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$9" x="-5.08" y="-5.08" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$10" x="-5.08" y="-6.35" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$11" x="2.54" y="-6.35" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$12" x="2.54" y="-5.08" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$13" x="2.54" y="-3.81" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$14" x="2.54" y="-2.54" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$15" x="2.54" y="-1.27" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$16" x="2.54" y="0" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$17" x="2.54" y="1.27" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$18" x="2.54" y="2.54" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$19" x="2.54" y="3.81" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$20" x="2.54" y="5.08" dx="2.54" dy="0.508" layer="1"/>
<wire x1="-5.08" y1="-7.62" x2="-5.08" y2="6.35" width="0.254" layer="51"/>
<wire x1="-5.08" y1="6.35" x2="2.54" y2="6.35" width="0.254" layer="51"/>
<wire x1="2.54" y1="6.35" x2="2.54" y2="-7.62" width="0.254" layer="51"/>
<wire x1="2.54" y1="-7.62" x2="-5.08" y2="-7.62" width="0.254" layer="51"/>
<text x="-5.08" y="6.35" size="1.27" layer="51">&gt;NAME</text>
<text x="-5.08" y="-8.89" size="1.27" layer="51">&gt;VALUE</text>
</package>
<package name="TAJ1020T">
<smd name="P$1" x="-6.35" y="5.08" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$2" x="-6.35" y="3.81" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$3" x="-6.35" y="2.54" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$4" x="-6.35" y="1.27" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$5" x="0" y="1.27" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$6" x="0" y="2.54" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$7" x="0" y="3.81" dx="2.54" dy="0.508" layer="1"/>
<smd name="P$8" x="0" y="5.08" dx="2.54" dy="0.508" layer="1"/>
<wire x1="-6.35" y1="6.35" x2="-6.35" y2="0" width="0.127" layer="51"/>
<wire x1="-6.35" y1="0" x2="0" y2="0" width="0.127" layer="51"/>
<wire x1="0" y1="0" x2="0" y2="6.35" width="0.127" layer="51"/>
<wire x1="0" y1="6.35" x2="-6.35" y2="6.35" width="0.127" layer="51"/>
<text x="-6.35" y="6.35" size="1.27" layer="51">&gt;NAME</text>
<text x="-6.35" y="-1.27" size="1.27" layer="51">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="ATTINY167">
<wire x1="-15.24" y1="12.7" x2="-15.24" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-15.24" x2="15.24" y2="-15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="-15.24" x2="15.24" y2="12.7" width="0.254" layer="94"/>
<wire x1="15.24" y1="12.7" x2="-15.24" y2="12.7" width="0.254" layer="94"/>
<pin name="(RXLIN)PA0" x="-20.32" y="10.16" length="middle"/>
<pin name="(TXLIN)PA1" x="-20.32" y="7.62" length="middle"/>
<pin name="(MISO)PA2" x="-20.32" y="5.08" length="middle"/>
<pin name="PA3" x="-20.32" y="2.54" length="middle"/>
<pin name="AVCC" x="-20.32" y="0" length="middle"/>
<pin name="AGND" x="-20.32" y="-2.54" length="middle"/>
<pin name="(MOSI)PA4" x="-20.32" y="-5.08" length="middle"/>
<pin name="(SCK)PA5" x="-20.32" y="-7.62" length="middle"/>
<pin name="PA6" x="-20.32" y="-10.16" length="middle"/>
<pin name="PA7" x="-20.32" y="-12.7" length="middle"/>
<pin name="(RESET)PB7" x="20.32" y="-12.7" length="middle" rot="R180"/>
<pin name="PB6" x="20.32" y="-10.16" length="middle" rot="R180"/>
<pin name="PB5" x="20.32" y="-7.62" length="middle" rot="R180"/>
<pin name="PB4" x="20.32" y="-5.08" length="middle" rot="R180"/>
<pin name="VCC" x="20.32" y="-2.54" length="middle" rot="R180"/>
<pin name="GND" x="20.32" y="0" length="middle" rot="R180"/>
<pin name="PB3" x="20.32" y="2.54" length="middle" rot="R180"/>
<pin name="PB2" x="20.32" y="5.08" length="middle" rot="R180"/>
<pin name="PB1" x="20.32" y="7.62" length="middle" rot="R180"/>
<pin name="PB0" x="20.32" y="10.16" length="middle" rot="R180"/>
<text x="-15.24" y="12.7" size="1.778" layer="94">&gt;NAME</text>
<text x="-15.24" y="12.7" size="1.778" layer="94">&gt;NAME</text>
<text x="-15.24" y="-17.78" size="1.778" layer="94">&gt;VALUE</text>
</symbol>
<symbol name="TAJ1020T">
<wire x1="-7.62" y1="15.24" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="15.24" width="0.254" layer="94"/>
<wire x1="7.62" y1="15.24" x2="-7.62" y2="15.24" width="0.254" layer="94"/>
<pin name="RXD" x="-12.7" y="12.7" length="middle"/>
<pin name="NSLP" x="-12.7" y="7.62" length="middle"/>
<pin name="NWAKE" x="-12.7" y="2.54" length="middle"/>
<pin name="TXD" x="-12.7" y="-2.54" length="middle"/>
<pin name="GND" x="12.7" y="-2.54" length="middle" rot="R180"/>
<pin name="LIN" x="12.7" y="2.54" length="middle" rot="R180"/>
<pin name="BAT" x="12.7" y="7.62" length="middle" rot="R180"/>
<pin name="INH" x="12.7" y="12.7" length="middle" rot="R180"/>
<text x="-7.62" y="15.24" size="1.778" layer="94">&gt;NAME</text>
<text x="-7.62" y="-7.62" size="1.778" layer="94">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATTINY167">
<gates>
<gate name="G$1" symbol="ATTINY167" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ATTINY167">
<connects>
<connect gate="G$1" pin="(MISO)PA2" pad="P$3"/>
<connect gate="G$1" pin="(MOSI)PA4" pad="P$7"/>
<connect gate="G$1" pin="(RESET)PB7" pad="P$11"/>
<connect gate="G$1" pin="(RXLIN)PA0" pad="P$1"/>
<connect gate="G$1" pin="(SCK)PA5" pad="P$8"/>
<connect gate="G$1" pin="(TXLIN)PA1" pad="P$2"/>
<connect gate="G$1" pin="AGND" pad="P$6"/>
<connect gate="G$1" pin="AVCC" pad="P$5"/>
<connect gate="G$1" pin="GND" pad="P$16"/>
<connect gate="G$1" pin="PA3" pad="P$4"/>
<connect gate="G$1" pin="PA6" pad="P$9"/>
<connect gate="G$1" pin="PA7" pad="P$10"/>
<connect gate="G$1" pin="PB0" pad="P$20"/>
<connect gate="G$1" pin="PB1" pad="P$19"/>
<connect gate="G$1" pin="PB2" pad="P$18"/>
<connect gate="G$1" pin="PB3" pad="P$17"/>
<connect gate="G$1" pin="PB4" pad="P$14"/>
<connect gate="G$1" pin="PB5" pad="P$13"/>
<connect gate="G$1" pin="PB6" pad="P$12"/>
<connect gate="G$1" pin="VCC" pad="P$15"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TJA1020T">
<gates>
<gate name="G$1" symbol="TAJ1020T" x="0" y="-5.08"/>
</gates>
<devices>
<device name="" package="TAJ1020T">
<connects>
<connect gate="G$1" pin="BAT" pad="P$7"/>
<connect gate="G$1" pin="GND" pad="P$5"/>
<connect gate="G$1" pin="INH" pad="P$8"/>
<connect gate="G$1" pin="LIN" pad="P$6"/>
<connect gate="G$1" pin="NSLP" pad="P$2"/>
<connect gate="G$1" pin="NWAKE" pad="P$3"/>
<connect gate="G$1" pin="RXD" pad="P$1"/>
<connect gate="G$1" pin="TXD" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead" urn="urn:adsk.eagle:library:325">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="2X05" urn="urn:adsk.eagle:footprint:22358/1" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-6.35" y1="-1.905" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-1.905" x2="-6.35" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="1.905" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.35" y2="1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="1.905" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-5.08" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="5.08" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="5.08" y="1.27" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-1.524" x2="-4.826" y2="-1.016" layer="51"/>
<rectangle x1="-5.334" y1="1.016" x2="-4.826" y2="1.524" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<rectangle x1="4.826" y1="1.016" x2="5.334" y2="1.524" layer="51"/>
<rectangle x1="4.826" y1="-1.524" x2="5.334" y2="-1.016" layer="51"/>
</package>
<package name="2X05/90" urn="urn:adsk.eagle:footprint:22359/1" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-6.35" y1="-1.905" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="6.985" x2="-5.08" y2="1.27" width="0.762" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="6.985" x2="-2.54" y2="1.27" width="0.762" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="6.985" x2="0" y2="1.27" width="0.762" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="6.985" x2="2.54" y2="1.27" width="0.762" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-1.905" x2="6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0.635" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="6.985" x2="5.08" y2="1.27" width="0.762" layer="21"/>
<pad name="2" x="-5.08" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="4" x="-2.54" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="6" x="0" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="8" x="2.54" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="10" x="5.08" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="1" x="-5.08" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="3" x="-2.54" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="5" x="0" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="7" x="2.54" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="9" x="5.08" y="-6.35" drill="1.016" shape="octagon"/>
<text x="-6.985" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="8.255" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-5.461" y1="0.635" x2="-4.699" y2="1.143" layer="21"/>
<rectangle x1="-2.921" y1="0.635" x2="-2.159" y2="1.143" layer="21"/>
<rectangle x1="-0.381" y1="0.635" x2="0.381" y2="1.143" layer="21"/>
<rectangle x1="2.159" y1="0.635" x2="2.921" y2="1.143" layer="21"/>
<rectangle x1="4.699" y1="0.635" x2="5.461" y2="1.143" layer="21"/>
<rectangle x1="-5.461" y1="-2.921" x2="-4.699" y2="-1.905" layer="21"/>
<rectangle x1="-2.921" y1="-2.921" x2="-2.159" y2="-1.905" layer="21"/>
<rectangle x1="-5.461" y1="-5.461" x2="-4.699" y2="-4.699" layer="21"/>
<rectangle x1="-5.461" y1="-4.699" x2="-4.699" y2="-2.921" layer="51"/>
<rectangle x1="-2.921" y1="-4.699" x2="-2.159" y2="-2.921" layer="51"/>
<rectangle x1="-2.921" y1="-5.461" x2="-2.159" y2="-4.699" layer="21"/>
<rectangle x1="-0.381" y1="-2.921" x2="0.381" y2="-1.905" layer="21"/>
<rectangle x1="2.159" y1="-2.921" x2="2.921" y2="-1.905" layer="21"/>
<rectangle x1="-0.381" y1="-5.461" x2="0.381" y2="-4.699" layer="21"/>
<rectangle x1="-0.381" y1="-4.699" x2="0.381" y2="-2.921" layer="51"/>
<rectangle x1="2.159" y1="-4.699" x2="2.921" y2="-2.921" layer="51"/>
<rectangle x1="2.159" y1="-5.461" x2="2.921" y2="-4.699" layer="21"/>
<rectangle x1="4.699" y1="-2.921" x2="5.461" y2="-1.905" layer="21"/>
<rectangle x1="4.699" y1="-5.461" x2="5.461" y2="-4.699" layer="21"/>
<rectangle x1="4.699" y1="-4.699" x2="5.461" y2="-2.921" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="2X05" urn="urn:adsk.eagle:package:22470/2" type="model" library_version="3">
<description>PIN HEADER</description>
</package3d>
<package3d name="2X05/90" urn="urn:adsk.eagle:package:22471/2" type="model" library_version="3">
<description>PIN HEADER</description>
</package3d>
</packages3d>
<symbols>
<symbol name="PINH2X5" urn="urn:adsk.eagle:symbol:22357/1" library_version="3">
<wire x1="-6.35" y1="-7.62" x2="8.89" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-7.62" x2="8.89" y2="7.62" width="0.4064" layer="94"/>
<wire x1="8.89" y1="7.62" x2="-6.35" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="7.62" x2="-6.35" y2="-7.62" width="0.4064" layer="94"/>
<text x="-6.35" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-2X5" urn="urn:adsk.eagle:component:22531/3" prefix="JP" uservalue="yes" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINH2X5" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X05">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22470/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="2X05/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22471/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="MASTER" library="senior_project" deviceset="ATTINY167" device=""/>
<part name="PROG1" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-2X5" device="" package3d_urn="urn:adsk.eagle:package:22470/2" value="SPI1"/>
<part name="SENSOR" library="senior_project" deviceset="ATTINY167" device=""/>
<part name="PROG2" library="pinhead" library_urn="urn:adsk.eagle:library:325" deviceset="PINHD-2X5" device="" package3d_urn="urn:adsk.eagle:package:22470/2" value="SPI2"/>
<part name="U$1" library="senior_project" deviceset="TJA1020T" device=""/>
<part name="U$2" library="senior_project" deviceset="TJA1020T" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="MASTER" gate="G$1" x="139.7" y="50.8"/>
<instance part="PROG1" gate="A" x="139.7" y="-10.16" rot="R180"/>
<instance part="SENSOR" gate="G$1" x="223.52" y="50.8"/>
<instance part="PROG2" gate="A" x="226.06" y="-10.16" rot="R180"/>
<instance part="U$1" gate="G$1" x="137.16" y="12.7"/>
<instance part="U$2" gate="G$1" x="223.52" y="12.7"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="MASTER" gate="G$1" pin="GND"/>
<wire x1="160.02" y1="50.8" x2="170.18" y2="50.8" width="0.1524" layer="91"/>
<label x="165.1" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MASTER" gate="G$1" pin="AGND"/>
<wire x1="119.38" y1="48.26" x2="109.22" y2="48.26" width="0.1524" layer="91"/>
<label x="109.22" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PROG1" gate="A" pin="1"/>
<wire x1="142.24" y1="-15.24" x2="154.94" y2="-15.24" width="0.1524" layer="91"/>
<label x="149.86" y="-15.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SENSOR" gate="G$1" pin="GND"/>
<wire x1="243.84" y1="50.8" x2="254" y2="50.8" width="0.1524" layer="91"/>
<label x="248.92" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SENSOR" gate="G$1" pin="AGND"/>
<wire x1="203.2" y1="48.26" x2="193.04" y2="48.26" width="0.1524" layer="91"/>
<label x="193.04" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PROG2" gate="A" pin="1"/>
<wire x1="228.6" y1="-15.24" x2="241.3" y2="-15.24" width="0.1524" layer="91"/>
<label x="236.22" y="-15.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND"/>
<wire x1="149.86" y1="10.16" x2="157.48" y2="10.16" width="0.1524" layer="91"/>
<label x="152.4" y="10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="GND"/>
<wire x1="236.22" y1="10.16" x2="243.84" y2="10.16" width="0.1524" layer="91"/>
<label x="238.76" y="10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="MASTER" gate="G$1" pin="VCC"/>
<wire x1="160.02" y1="48.26" x2="172.72" y2="48.26" width="0.1524" layer="91"/>
<label x="165.1" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MASTER" gate="G$1" pin="AVCC"/>
<wire x1="119.38" y1="50.8" x2="109.22" y2="50.8" width="0.1524" layer="91"/>
<label x="109.22" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PROG1" gate="A" pin="9"/>
<wire x1="142.24" y1="-5.08" x2="154.94" y2="-5.08" width="0.1524" layer="91"/>
<label x="149.86" y="-5.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SENSOR" gate="G$1" pin="VCC"/>
<wire x1="243.84" y1="48.26" x2="256.54" y2="48.26" width="0.1524" layer="91"/>
<label x="248.92" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SENSOR" gate="G$1" pin="AVCC"/>
<wire x1="203.2" y1="50.8" x2="193.04" y2="50.8" width="0.1524" layer="91"/>
<label x="193.04" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PROG2" gate="A" pin="9"/>
<wire x1="228.6" y1="-5.08" x2="241.3" y2="-5.08" width="0.1524" layer="91"/>
<label x="236.22" y="-5.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="BAT"/>
<wire x1="149.86" y1="20.32" x2="157.48" y2="20.32" width="0.1524" layer="91"/>
<label x="152.4" y="20.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="BAT"/>
<wire x1="236.22" y1="20.32" x2="243.84" y2="20.32" width="0.1524" layer="91"/>
<label x="238.76" y="20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="PROG1" gate="A" pin="10"/>
<wire x1="134.62" y1="-5.08" x2="124.46" y2="-5.08" width="0.1524" layer="91"/>
<label x="124.46" y="-5.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MASTER" gate="G$1" pin="(MOSI)PA4"/>
<wire x1="119.38" y1="45.72" x2="109.22" y2="45.72" width="0.1524" layer="91"/>
<label x="109.22" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="RST" class="0">
<segment>
<pinref part="PROG1" gate="A" pin="6"/>
<wire x1="134.62" y1="-10.16" x2="124.46" y2="-10.16" width="0.1524" layer="91"/>
<label x="124.46" y="-10.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MASTER" gate="G$1" pin="(RESET)PB7"/>
<wire x1="170.18" y1="38.1" x2="160.02" y2="38.1" width="0.1524" layer="91"/>
<label x="165.1" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="PROG1" gate="A" pin="4"/>
<wire x1="134.62" y1="-12.7" x2="124.46" y2="-12.7" width="0.1524" layer="91"/>
<label x="124.46" y="-12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MASTER" gate="G$1" pin="(SCK)PA5"/>
<wire x1="119.38" y1="43.18" x2="109.22" y2="43.18" width="0.1524" layer="91"/>
<label x="109.22" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="MASTER" gate="G$1" pin="(MISO)PA2"/>
<wire x1="119.38" y1="55.88" x2="109.22" y2="55.88" width="0.1524" layer="91"/>
<label x="109.22" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PROG1" gate="A" pin="2"/>
<wire x1="134.62" y1="-15.24" x2="124.46" y2="-15.24" width="0.1524" layer="91"/>
<label x="124.46" y="-15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="RXD"/>
<wire x1="124.46" y1="25.4" x2="96.52" y2="25.4" width="0.1524" layer="91"/>
<pinref part="MASTER" gate="G$1" pin="(RXLIN)PA0"/>
<wire x1="96.52" y1="25.4" x2="96.52" y2="60.96" width="0.1524" layer="91"/>
<wire x1="96.52" y1="60.96" x2="119.38" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="MASTER" gate="G$1" pin="(TXLIN)PA1"/>
<wire x1="119.38" y1="58.42" x2="99.06" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="TXD"/>
<wire x1="99.06" y1="58.42" x2="99.06" y2="10.16" width="0.1524" layer="91"/>
<wire x1="99.06" y1="10.16" x2="124.46" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="INH"/>
<wire x1="149.86" y1="25.4" x2="157.48" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="LIN"/>
<wire x1="149.86" y1="15.24" x2="177.8" y2="15.24" width="0.1524" layer="91"/>
<wire x1="177.8" y1="15.24" x2="177.8" y2="2.54" width="0.1524" layer="91"/>
<wire x1="177.8" y1="2.54" x2="246.38" y2="2.54" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="LIN"/>
<wire x1="246.38" y1="2.54" x2="246.38" y2="15.24" width="0.1524" layer="91"/>
<wire x1="246.38" y1="15.24" x2="236.22" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="INH"/>
<wire x1="236.22" y1="25.4" x2="243.84" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="TXD"/>
<wire x1="210.82" y1="10.16" x2="190.5" y2="10.16" width="0.1524" layer="91"/>
<wire x1="190.5" y1="10.16" x2="190.5" y2="58.42" width="0.1524" layer="91"/>
<pinref part="SENSOR" gate="G$1" pin="(TXLIN)PA1"/>
<wire x1="190.5" y1="58.42" x2="203.2" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="RXD"/>
<wire x1="210.82" y1="25.4" x2="187.96" y2="25.4" width="0.1524" layer="91"/>
<pinref part="SENSOR" gate="G$1" pin="(RXLIN)PA0"/>
<wire x1="187.96" y1="25.4" x2="187.96" y2="60.96" width="0.1524" layer="91"/>
<wire x1="187.96" y1="60.96" x2="203.2" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MISO2" class="0">
<segment>
<pinref part="SENSOR" gate="G$1" pin="(MISO)PA2"/>
<wire x1="203.2" y1="55.88" x2="193.04" y2="55.88" width="0.1524" layer="91"/>
<label x="193.04" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PROG2" gate="A" pin="2"/>
<wire x1="220.98" y1="-15.24" x2="210.82" y2="-15.24" width="0.1524" layer="91"/>
<label x="210.82" y="-15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="MOSI2" class="0">
<segment>
<pinref part="SENSOR" gate="G$1" pin="(MOSI)PA4"/>
<wire x1="203.2" y1="45.72" x2="193.04" y2="45.72" width="0.1524" layer="91"/>
<label x="193.04" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PROG2" gate="A" pin="10"/>
<wire x1="220.98" y1="-5.08" x2="210.82" y2="-5.08" width="0.1524" layer="91"/>
<label x="210.82" y="-5.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="SCK2" class="0">
<segment>
<pinref part="SENSOR" gate="G$1" pin="(SCK)PA5"/>
<wire x1="203.2" y1="43.18" x2="193.04" y2="43.18" width="0.1524" layer="91"/>
<label x="193.04" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PROG2" gate="A" pin="4"/>
<wire x1="220.98" y1="-12.7" x2="210.82" y2="-12.7" width="0.1524" layer="91"/>
<label x="210.82" y="-12.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="RST2" class="0">
<segment>
<pinref part="SENSOR" gate="G$1" pin="(RESET)PB7"/>
<wire x1="254" y1="38.1" x2="243.84" y2="38.1" width="0.1524" layer="91"/>
<label x="248.92" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="PROG2" gate="A" pin="6"/>
<wire x1="220.98" y1="-10.16" x2="210.82" y2="-10.16" width="0.1524" layer="91"/>
<label x="210.82" y="-10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="SENIN(NO)" class="0">
<segment>
<pinref part="SENSOR" gate="G$1" pin="PA3"/>
<wire x1="203.2" y1="53.34" x2="185.42" y2="53.34" width="0.1524" layer="91"/>
<wire x1="185.42" y1="53.34" x2="185.42" y2="76.2" width="0.1524" layer="91"/>
<label x="185.42" y="73.66" size="1.778" layer="95" rot="R270"/>
<wire x1="185.42" y1="76.2" x2="175.26" y2="76.2" width="0.1524" layer="91"/>
<wire x1="175.26" y1="76.2" x2="175.26" y2="45.72" width="0.1524" layer="91"/>
<pinref part="MASTER" gate="G$1" pin="PB4"/>
<wire x1="175.26" y1="45.72" x2="160.02" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ENDIN" class="0">
<segment>
<pinref part="SENSOR" gate="G$1" pin="PB0"/>
<wire x1="243.84" y1="60.96" x2="248.92" y2="60.96" width="0.1524" layer="91"/>
<wire x1="248.92" y1="60.96" x2="248.92" y2="78.74" width="0.1524" layer="91"/>
<label x="246.38" y="73.66" size="1.778" layer="95" rot="R270"/>
<pinref part="MASTER" gate="G$1" pin="PB3"/>
<wire x1="172.72" y1="78.74" x2="172.72" y2="53.34" width="0.1524" layer="91"/>
<wire x1="172.72" y1="53.34" x2="160.02" y2="53.34" width="0.1524" layer="91"/>
<wire x1="248.92" y1="78.74" x2="172.72" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ENDOUT" class="0">
<segment>
<wire x1="251.46" y1="58.42" x2="251.46" y2="81.28" width="0.1524" layer="91"/>
<pinref part="SENSOR" gate="G$1" pin="PB1"/>
<wire x1="243.84" y1="58.42" x2="251.46" y2="58.42" width="0.1524" layer="91"/>
<label x="251.46" y="73.66" size="1.778" layer="95" rot="R270"/>
<pinref part="MASTER" gate="G$1" pin="PB2"/>
<wire x1="160.02" y1="55.88" x2="170.18" y2="55.88" width="0.1524" layer="91"/>
<wire x1="251.46" y1="81.28" x2="170.18" y2="81.28" width="0.1524" layer="91"/>
<wire x1="170.18" y1="81.28" x2="170.18" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LINOUT" class="0">
<segment>
<pinref part="SENSOR" gate="G$1" pin="PB6"/>
<wire x1="243.84" y1="40.64" x2="261.62" y2="40.64" width="0.1524" layer="91"/>
<wire x1="261.62" y1="40.64" x2="261.62" y2="86.36" width="0.1524" layer="91"/>
<label x="259.08" y="73.66" size="1.778" layer="95" rot="R270"/>
<wire x1="165.1" y1="86.36" x2="261.62" y2="86.36" width="0.1524" layer="91"/>
<pinref part="MASTER" gate="G$1" pin="PB0"/>
<wire x1="160.02" y1="60.96" x2="165.1" y2="60.96" width="0.1524" layer="91"/>
<wire x1="165.1" y1="86.36" x2="165.1" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED" class="0">
<segment>
<pinref part="SENSOR" gate="G$1" pin="PB2"/>
<wire x1="243.84" y1="55.88" x2="254" y2="55.88" width="0.1524" layer="91"/>
<wire x1="254" y1="55.88" x2="254" y2="83.82" width="0.1524" layer="91"/>
<label x="254" y="73.66" size="1.778" layer="95" rot="R270"/>
<pinref part="MASTER" gate="G$1" pin="PB1"/>
<wire x1="160.02" y1="58.42" x2="167.64" y2="58.42" width="0.1524" layer="91"/>
<wire x1="167.64" y1="58.42" x2="167.64" y2="83.82" width="0.1524" layer="91"/>
<wire x1="167.64" y1="83.82" x2="254" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
