/*
 * SensorTester.c
 *
 * Created: 1/18/2018 12:25:54 PM
 * Author : Nathan
 */ 

#define F_CPU 8000000UL
#include <stdlib.h>
#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "lindrv/lin_drv.h"
#include "dbg/dbg_putchar.h"

#define DEFAULT_ID		0x10
#define DEFAULT_LEN		1

#define Is_lin_switch_high()	(PINB & (1<<PINB0))
#define Is_led_output_high()	(PINB & (1<<PINB1))
#define Is_end_switch_high()	(PINB & (1<<PINB2))

#define Set_end_high()			PORTB |= (1<<PINB3)
#define Set_end_low()			PORTB &= ~(1<<PINB3)
#define Set_sensor_high()		PORTB |= (1<<PINB4)
#define Set_sensor_low()		PORTB &= ~(1<<PINB4)
#define Set_power_high()		PORTB |= (1<<PINB6)
#define Set_power_low()			PORTB &= ~(1<<PINB6)
	
static volatile uint8_t Timer1;

/************************************************************************/
/* Helper Functions                                                     */
/************************************************************************/

uint8_t lin_exchange(uint8_t id ) {
	lin_tx_header(LIN_2X, id, 1);
	Timer1 = 20;
	while(Timer1 && !(Is_lin_header_ready())) {}
	Lin_clear_idok_it();
	
	lin_rx_response(LIN_2X, 1);

	Timer1 = 20;
	while(Timer1 && !(Is_lin_rx_response_ready())) {}

	uint8_t response[1] = {0};
	if (Is_lin_rx_response_ready()) {
		lin_get_response(response);
	}

	Lin_clear_rxok_it();
	return response[0];
}

uint8_t get_response(uint8_t id) {
	_delay_ms(1000);
	uint8_t resp = 0;
	char str[2];

	dbg_putstr_P(PSTR("    Sending Header: "));
	utoa(id,str,10);
	dbg_putstr(str);	
	dbg_putstr_P(PSTR("\n"));

	resp = lin_exchange(id);

	utoa(resp, str, 10);
	dbg_putstr_P(PSTR("    Response: "));
	dbg_putstr(str);
	dbg_putstr_P(PSTR("\n"));

	return resp;
}

/************************************************************************/
/* Test Functions                                                       */
/************************************************************************/

void test_startup_end(void) {
	Set_sensor_high();
	Set_end_low();
	Set_power_high();
	_delay_ms(1000);

	/* Lin to next sensor should be disabled */
	if (Is_lin_switch_high())						dbg_putstr_P(PSTR("  X - Lin switch shouldn't be on\n"));

	/* End switch is always on */
	if(!Is_end_switch_high())						dbg_putstr_P(PSTR("  X - End switch should be on\n"));

	/* LED should be on while initializing */
	if(!Is_led_output_high())						dbg_putstr_P(PSTR("  X - LED should be on while initializing\n"));

}
void test_startup_mid() {
	Set_sensor_high();
	Set_end_high();
	Set_power_high();
	_delay_ms(1000);
	
	/* Lin to next sensor should be disabled */
	if (Is_lin_switch_high())						dbg_putstr_P(PSTR("  X - Lin switch shouldn't be on\n"));
	
	/* End switch is always on */
	if(!Is_end_switch_high())						dbg_putstr_P(PSTR("  X - End switch should be on\n"));

	/* LED should be on while initializing */
	if(!Is_led_output_high())						dbg_putstr_P(PSTR("  X - LED should be on while initializing\n"));
}

void test_id_initialization_end(void) {
	
	/* Should not respond to 0x00 on LIN */
	if(get_response(0x00))							dbg_putstr_P(PSTR("  X - ID not initialized, shouldn't respond to 0\n"));
	_delay_ms(500);

	/* Should initialize with ID = 0x01 */
	if(get_response(DEFAULT_ID) != DEFAULT_ID)		dbg_putstr_P(PSTR("  X - Did not initialize correctly\n"));
	_delay_ms(500);

	/* Is last sensor, should respond with FF to next header */
	if(get_response(0x02) != 0xFF)					dbg_putstr_P(PSTR("  X - Did not end initialization sequence\n"));
}

void test_id_initialization_mid(void) {
	/* Should not respond to 0x00 on LIN */
	if(get_response(0x00))							dbg_putstr_P(PSTR("  X - ID not initialized, shouldn't respond to 0\n"));
	_delay_ms(500);

	/* Should initialize with ID = 0x01 */
	if(get_response(DEFAULT_ID) != DEFAULT_ID)		dbg_putstr_P(PSTR("  X - Did not initialize correctly\n"));
	_delay_ms(500);

	/* Shouldn't respond to any further initialization IDs*/
	if(get_response(0x02))							dbg_putstr_P(PSTR("  X - Should not respond until ID called\n"));
	_delay_ms(500);

}


void test_sensor_operation(void) {
	// Sensor updates after every poll
	
	for(int i=0; i<1; i++) {
	_delay_ms(500);
	Set_sensor_high();
	/* Sensor is low, grain detected */
	if(Is_led_output_high())						dbg_putstr_P(PSTR("  X - LED output should be high with grain present\n"));
	/* LIN response should be 0xFF */
	if(!get_response(DEFAULT_ID))					dbg_putstr_P(PSTR("  X - Grain detected, LIN response should be 0xFF\n"));

	_delay_ms(500);
	Set_sensor_low();
	
	/* Sensor is high, no grain detected */
	if(!Is_led_output_high())						dbg_putstr_P(PSTR("  X - LED output shouldn't be high with no grain present\n"));
	/* LIN response should be 0x00 */
	if(get_response(DEFAULT_ID))					dbg_putstr_P(PSTR("  X - No grain, LIN response should be 0x00"));
	
	
	}

}

/* Test the complete operation of the sensor */
void test_end(void) {
	dbg_putstr_P(PSTR("Testing Sensor as END\n"));
	test_startup_end();
	test_id_initialization_end();
	test_sensor_operation();
	Set_power_low();
}
/* Test up through initialization */
void test_mid(void) {
	dbg_putstr_P(PSTR("Testing Sensor as MID\n"));
	test_startup_mid();
	test_id_initialization_mid();
	Set_power_low();
}

/************************************************************************/
/* Main                                                     */
/************************************************************************/

/* update timer every 10ms */
ISR(TIMER1_COMPA_vect) {
	if(Timer1) --Timer1;
}

void setup(void) {
	dbg_tx_init();
	dbg_putstr("Initializing\n");
	
	lin_init(LIN_2X, CONF_LINBRR);
	
	//		Lin out		 LED out	  End out	   End in		Sensor In	 Power
	DDRB |= (0<<PINB0) | (0<<PINB1) | (0<<PINB2) | (1<<PINB3) | (1<<PINB4) | (0<<PINB6);

	// Timer1		CTC							Clk/64
	TCCR1B = ((1 << WGM12) | 0 << CS12) | (1 << CS11) | (1 << CS10);
	TIMSK1 |= (1 << OCIE1A);
	// 10ms
	OCR1A = 1250;
	asm ("sei");

	dbg_putstr_P(PSTR("Sensor Tester Initialized\n\n"));
}

int main(void) {
	setup();
	_delay_ms(1000);
	dbg_putstr_P(PSTR("Starting Tests\n\n"));
	
	//test_mid();
	test_end();
	
	dbg_putstr_P(PSTR("Finished Testing\n"));
	while(1) {

	}
	return 0;
}
