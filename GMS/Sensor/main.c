
/*
 * Sensor.c
 *
 * Created: 2/14/2018 5:49:55 PM
 * Author : Nathan
 */ 

#define F_CPU 8000000UL

#define NODETECT 0x55;
#define DETECT 0xAA;

#define STATE_NORM		0
#define STATE_INIT		1
#define STATE_INIT_END	2

#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "lindrv/lin_drv.h"
#include "dbg/dbg_putchar.h"


unsigned char id[1] = {0x00};
unsigned char status[1] = {0x55};
volatile int state;

void print_v(uint8_t val) {
	char string[10];
	utoa(val, string, 10);
	dbg_putstr(string);
	dbg_putstr_P(PSTR("\n"));
}

/* Respond to LIN ID */
void lin_id_received(void) {
	uint8_t id_received = Lin_get_id();
	switch (state) {
		case STATE_NORM:								// Normal Operation
			if(id_received == id[0]) {
				lin_tx_response(LIN_2X, status, 1);
			}
			break;

		case STATE_INIT:								// Get ID
			if(id_received == 0x00) break;
			id[0] = id_received;
			lin_tx_response(LIN_2X, id, 1);
			state = (PINB & (1<<PINB1)) ? STATE_NORM : STATE_INIT_END ;
			_delay_ms(200);
			PORTB |= (1<<PINB6);
			
			dbg_putstr_P(PSTR("My ID: "));
			print_v(id_received);
			break;

		case STATE_INIT_END:							// Respond with 0xFE to end init
			lin_tx_response(LIN_2X, (unsigned char *) 0xFE, 1);
			PORTB &= ~(1<<PINB2);
			state = STATE_NORM;
			break;
	}
	
	dbg_putstr_P(PSTR("H: "));
	print_v(id_received);
	dbg_putstr_P(PSTR("STATE: "));
	print_v(state);
}

/* Update based on sensor input*/
void update(void) {
	if(state == STATE_NORM){
		if(PINA & (1<<PINA3)) {
			// No grain detected, status low, LED off
			status[0] = NODETECT;
			PORTB &= ~(1<<PORTB2);
		} else {
			// Grain detected, status high, LED on
			status[0] = DETECT;
			PORTB |= (1<<PORTB2);
		}
	}
}

/* Initialize pin setup */
void setup(void) {
	state = STATE_INIT;

	//		 SENSOR IN
	DDRA &= ~(1<<DDA3);
	//		 END OUT		LED OUT		 LIN OUT
	DDRB |= (1<<DDB0)  | (1<<DDB2) | (1<<DDB6);
	DDRB &= ~(1<<DDB1);
	
	// Enable LIN
	lin_init(LIN_2X, CONF_LINBRR);
	
	dbg_tx_init();
	dbg_putstr_P(PSTR("Initializing\n"));
	
	// Set ENDOUT and LED OUT to HIGH
	PORTB |= (1<<PINB0) | (1<<PINB2);

	_delay_ms(1000);
	dbg_putstr_P(PSTR("PINB1 Read: "));
	print_v((PINB & (1<<PINB1))? 1: 0 );
}

int main(void) {
	setup();
    while (1) {
		switch (Lin_get_it()) {
			case LIN_IDOK:					// ID RECIEVED
				lin_id_received();
				Lin_clear_idok_it();
				break;
			case LIN_RXOK:					// TX RESPONSE RECIEVED (IGNORE)
				Lin_clear_rxok_it();
				break;
			case LIN_TXOK:					// READY FOR ID
				update();
				Lin_clear_txok_it();
				break;
			case LIN_ERROR:					// ERROR  (IGNORE)
				Lin_clear_err_it();
				break;
			default:
				break;
		}
    }
}

