# 2-3 Trees#

2-3 Trees are an implementation of a balanced search tree. 
Unlike a traditional binary search tree when give presorted data,
a 2-3 tree will keep the tree balanced, giving near log(n) lookups,
in comparison to a linear search tradtionally.