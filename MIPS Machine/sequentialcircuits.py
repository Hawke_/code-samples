# sequentialcircuits.py
# Nathan Frana
# Stage 3
# CS 360 - Winter 2017
from boolarray import BoolArray

class RegisterFile:
    def __init__(self):
        """
        # The register file is a list of 32 32-bit registers (BoolArray)
        # register 29 is initialized to "000003E0" the rest to "00000000"
        # an instance vaariable for writeReg is initialized to False
        """
        self.registers = {}
        for i in range(32):
            self.registers[i] = BoolArray('0',32)

        self.registers[29] = BoolArray('3E0',32)
        self.writeReg = False
        
    def readCycle(self,readReg1,readReg2,writeReg):
        """
        # readReg1, readReg2, and writeReg are integers 0..31
        # writeReg is 'remembered' in the instance variable
        # copies of the contents of the two read registers
        # are returned as BoolArrays in the given order
        """
        self.writeReg = writeReg
        return self.registers[readReg1], self.registers[readReg2]
        
    
    def writeCycle(self,RegWrite,data):
        """
        # RegWrite is a boolean indicating that a write should occur
        # data is a BoolArray to be written if RegWrite is true
        # if the RegWrite control is True,
        #    the value of data is copied into the writeReg register
        #    where writeReg is the value remembered from the most
        #    recent read cycle 
        # if RegWrite is false, no values change
        # NOTE: if writeReg is 0, nothing happens since $zero is
        #                         supposed to be the constant 0
        """
        if RegWrite and self.writeReg != 0:
            self.registers[self.writeReg] = data
      
        #self.registers[self.writeReg] = RegWrite and not int(self.writeReg) == 0 and data.copy()
      
    def getReg(self,i):
        """
        # returns a copy of the BoolArray in register i
        # this is for debug and display purposes
        """
        return self.registers[i]

    def showRegisters(self):
        """
        # prints the index and hex contents of all non-zero registers
        # this is for debug and display purposes
        """
        for key in self.registers.keys():
            (int(self.registers[key]) is not 0 and
             print(key," | ", self.registers[key].toHex(), " | ", int(self.registers[key]), " | ", self.registers[key]))

class Memory:
    """
    # Let's use a 1K memory. addresses 0..3FF
    # Locations 000003F0,000003F4,000003F8,and 000003FC represent
    #             inflag,  inbuff, outflag, and  outbuff respectively.   
    # sp is 3E0 and grows down toward 000
    """
    
    def __init__(self):
        """
        # creates a dictionary of BoolArrays whose
        #       keys are integers 0, 4, 8, ... , 1020 and
        #       values are all initially BoolArray("0",32)
        """
        self.memory = {}
        for i in range(0,1024,4):
            self.memory[i] = BoolArray('0',32)

    def __getitem__(self,i):
        return self.memory[i]

    def __setitem__(self,i,v):
        self.memory[i] = v
    
    def showMemory(self):
        """
        # print a nicely formatted table of non-zero memory
        """
        for key in self.memory.keys():
            (int(self.memory[key]) is not 0 and
             print(key, " | ", self.memory[key].toHex(), " | ", int(self.memory[key]), " | ", self.memory[key]))
    
    def loadProgram(self,fname):
        """
        # fname is the name of a text file that contains
        # one 8-digit hexidecimal string per line
        # they are to be converted to BoolArray and
        # stored sequentially into memory in
        # locations 0, 4, 8, 12, ...
        """
        file = open(fname,'r')
        i = 0
        for line in file:
            inst = line.replace('\n','')
            self.memory[i] = BoolArray(inst,32)
            i += 4
        file.close()
        
    def instrCycle(self,PC):
        """
        # PC is a BoolArray of length 32
        # returns a copy of the memory content at address: int(PC)
        """
        return self.memory[int(PC)]

    def dataCycle(self,Address,WriteData,ReadMem,WriteMem):
        """
        # Address and Write Data are BoolArrays
        # ReadMem and WriteMem are booleans (not both True)
        """
        assert not (WriteMem and ReadMem)
        
        """
        # if ReadMem True:
        #        return a copy of the BoolArray at mem[int(address)]
        # if WriteMem is True:
        #        a copy of WriteData is placed at mem[int(address)]
        #        a BoolArray("00000000",32) is returned
        """
        if ReadMem:
            return self.memory[int(Address)]

        if WriteMem:
            self.memory[int(Address)] = WriteData.copy()
            
        return BoolArray('0', 32)
        
        
