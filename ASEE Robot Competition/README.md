# ASEE Annual Robotics Competition #

In the second 7 weeks of my Electronics and Digital Systems class, we were given the task of building a robot to complete the 2017 American Society of Engineering Education's robotics challenge. Initially we were given a Parrallax Boe-Bot Kit and we set off on our own. 
Myself and a group of 3 other engineering students brainstormed an idea of what we should do. Being the best programmer, I was given the task of writing the code and navigation, another memeber was given the task of picking up barrels, the last two were in charge of dispensing barrels. We met daily to discuss ideas and to get input on our work. 
By the end of the seven weeks, we had a working robot that was able to complete the course. On April 11, 2017 we presented our robot in front of the community at Wartburg College's annual RICE day. 

