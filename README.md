# Code Samples #

These are code samples from class work and various private projects


##Assignments/Projects##
__Senior Project - Grain Level Monitoring System__
	[Link](GMS/)

__ASEE Robotics Competition__
	[Link](ASEE%20Robot%20Competition/)

__ MIPS Machine__
	[Link](MIPS%20Machine/)
	
__Dynamic coin problems__ 
	[Link](DynamicProgramming/)

__2-3 Tree Implementation__
	[Link](2-3Tree/)

##Private Projects##
__Mensa Menu App__
	[Link](WartburgMensaMenu/)

__TimeClock App__
	[Link](TimeClock/)
