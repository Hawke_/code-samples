#Nathan Frana
#Project 4
#Node class for 2-3 Tree
#Stores data and tests conditions for the values

class TreeNode():
    #p is the parent, r are the roots, c are the children
    p = None
    r = []
    c = []


    def __init__(self,r):
        self.p = None
        self.r = r
        self.c = [None,None,None,None]


    def hasNoRoot(self):
        if self.r[0] == None and self.r[1] == None and self.r[2] == None:
            return True
        return False
    
    def hasOneRoot(self):
        return self.r[0] != None and self.r[1] == None and self.r[2] == None
    
    def hasTwoRoot(self):
        return self.r[0] != None and self.r[1] != None and self.r[2] == None

    def hasThreeRoot(self):
        return self.r[0] != None and self.r[1] != None and self.r[2] != None

    def isLeaf(self):
        return self.c[0] == None and self.c[1] == None and self.c[2] == None


