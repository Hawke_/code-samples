package com.replay.timeclock.dbmanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.Settings;

import java.util.ArrayList;
import java.util.List;

/**
 * Singleton object, allows only one object across application
 */

public class SQLiteManager extends SQLiteOpenHelper {

    private Context context;
    private static final String DBNAME = "TimeClock.db";
    private static final int DBVERSION = 1;

    /*
    SQLite Create Queries
     */
    public static final String CREATE_JOBS_TABLE = "CREATE TABLE IF NOT EXISTS Jobs(" +
            "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "name TEXT, " +
            "hours REAL, " +
            "wage REAL" +
            ")";

    public static String CREATE_JOB_ENTRY_TABLE (String name) {
        return "CREATE TABLE IF NOT EXISTS '" + name + "'(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "startTime TEXT," +
                "endTime Text," +
                "hours REAL" +
                ")";
    }

    /*
    SQLiteOpenHelper Methods
     */
    private static SQLiteManager ourInstance;

    public static SQLiteManager getInstance(Context context) {
        if(ourInstance == null) {
            ourInstance = new SQLiteManager(context.getApplicationContext());
        }
        return ourInstance;
    }

    private SQLiteManager(Context context) {
        super(context,DBNAME, null, DBVERSION);
        this.context = context;


    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_JOBS_TABLE);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Jobs");
        onCreate(db);
    }

    /*
    Job Methods
     */
    public void addJob(Job job){
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", job.name);
        contentValues.put("hours", job.hours);
        contentValues.put("wage", job.wage);

        SQLiteDatabase db = this.getWritableDatabase();
        long id = db.insert("Jobs", null, contentValues);
        db.execSQL(CREATE_JOB_ENTRY_TABLE(Long.toString(id)));
        db.close();
    }

    public Job getJob(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query("Jobs", new String[] {"id","name","hours","wage"}, "id=?",
                new String[] {String.valueOf(id)},null,null,null,null);

        if(cursor != null) {
            cursor.moveToFirst();
        }
        Job job = new Job();
        job.id = cursor.getInt(0);
        job.name = cursor.getString(1);
        job.hours = cursor.getFloat(2);
        job.wage = cursor.getFloat(3);

        cursor.close();
        db.close();
        return job;
    }

    public List<Job> getAllJobs() {
        List<Job> jobList = new ArrayList<Job>();
        String selectQuery = "SELECT * FROM Jobs";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()) {
            do {
                Job job = new Job();
                job.id = cursor.getInt(0);
                job.name = cursor.getString(1);
                job.hours = cursor.getFloat(2);
                job.wage = cursor.getFloat(3);

                jobList.add(job);
            } while(cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return jobList;
    }

    public int updateJob(Job job) {

        ContentValues contentValues = new ContentValues();
        contentValues.put("id", job.id);
        contentValues.put("name", job.name);
        contentValues.put("hours", job.hours);
        contentValues.put("wage", job.wage);

        SQLiteDatabase db = this.getWritableDatabase();
        return db.update("Jobs", contentValues, "id = ?", new String[]{String.valueOf(job.id)});
    }

    public void deleteJob(Job job) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("Jobs", "id = ?", new String[]{String.valueOf(job.id)});
        db.close();
    }

    /*
    Entry Methods
     */
    public void addEntry(Job job, Entry entry){
        ContentValues contentValues = new ContentValues();
        contentValues.put("startTime", entry.startTime);
        contentValues.put("endTime", entry.endTime);
        contentValues.put("hours", entry.hours);

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert("'" + String.valueOf(job.id) + "'", null, contentValues);
        db.close();
    }

    public Entry getEntry(Job job, int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query("'" + String.valueOf(job.id) + "'", new String[]{"id","startTime","endTime","hours"}, "id=?",
                new String[]{String.valueOf(id)},null,null,null,null );

        if(cursor != null){
            cursor.moveToFirst();
        }
        Entry entry = new Entry();
        entry.id = cursor.getInt(0);
        entry.startTime = cursor.getString(1);
        entry.endTime = cursor.getString(2);
        entry.hours = cursor.getFloat(3);

        cursor.close();
        db.close();
        return entry;
    }

    public List<Entry> getAllEntries(Job job) {
        List<Entry> entryList = new ArrayList<Entry>();
        String selectQuery = "SELECT * FROM '" + job.id + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()) {
            do {
                Entry entry = new Entry();
                entry.id = cursor.getInt(0);
                entry.startTime = cursor.getString(1);
                entry.endTime = cursor.getString(2);
                entry.hours = cursor.getFloat(3);

                entryList.add(entry);
            } while(cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return entryList;
    }

    public int updateEntry(Job job, Entry entry) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("startTime", entry.startTime);
        contentValues.put("endTime", entry.endTime);
        contentValues.put("hours", entry.hours);

        SQLiteDatabase db = this.getWritableDatabase();
        return db.update("'" + String.valueOf(job.id) + "'", contentValues, "id = ?", new String[]{String.valueOf(entry.id)});
    }

    public void deleteEntry(Job job, Entry entry) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("'" + String.valueOf(job.id) + "'", "id = ?", new String[]{String.valueOf(entry.id)});
        db.close();
    }


}
