# TimeClock App #

During the fall, harvest season is in full swing. Being the son of a farmer, I spend my weekends at home running the grain cart while my father runs the combine. He also custom harvests for other farmers in the area, bringing me along. This means I needed a way to track my hours accurately, so I made this app. Utilizing a sqlite database for information storage, it allows for the creation and editing of different jobs and entries. Individual entries are editable and all the information can be exported to a CSV format in the users downloads folder.

In the future I'd like to add the following   
 + Proper handling of Android 6.0+ permissions    
 + A custom Date&Time Picker widget special to my use   
 + Persistent notification when recording, with buttons to pause/stop   
 + Styling to make it look nicer and more polished   
 + Possibly publish on the app store   