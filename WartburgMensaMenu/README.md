# Mensa Menu Android App #

This app was created to provide easy access to the school's online menu. Traditionally their implementation on their website is tedious to follow through, requiring multiple pages to be loaded just to get to the menu. This application simplies this, one click and the menu for the current day is show. Swiping left and right allow you to traverse through the days of the week. 


https://play.google.com/store/apps/details?id=com.replay.menu