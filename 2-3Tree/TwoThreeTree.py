#Nathan Frana
#Project 4
#2-3 Tree implementation

# If the node has one root, it has a left and right child
# If it has two roots, it has a left, middle, and right child
from TreeNode import TreeNode


class TwoThreeTree():

    treesize = 0
    root = None

    """
    pre:init optional elements to insert into the tree
    """
    def __init__(self,init=[]):

        for i in init:
            self.insert(i)

        
# Insert and split basic algorithm from:
# http://www.cs.colostate.edu/~anderson/cs200/index.html/doku.php?id=notes:balanced_binary_search_trees

    """
    Inserts value into the tree
    pre: value to be inserted
    """
    def insert(self,value):
        self.treesize += 1
        # If the tree is empty create new root
        if self.root == None:
            self.root = TreeNode([None,None,None])
            self.root.r[0] = value
        else:
            # Insert the value in the appropreate leaf
            node = self._find(value)
            if node.hasOneRoot():
                if value <= node.r[0]:
                    node.r = [value,node.r[0],None]
                else:
                    node.r = [node.r[0],value,None]
            else:
                if value <= node.r[0]:
                    node.r = [value,node.r[0],node.r[1]]
                elif value > node.r[0] and value <= node.r[1]:
                    node.r = [node.r[0],value,node.r[1]]
                else:
                    node.r = [node.r[0],node.r[1],value]
            # If after inserting, if there are 3 values in the root, split it
            if node.hasThreeRoot():
            
                self._split(node)
            
        
    """
    Splits any 3 node into two nodes and handles the children/parents
    """
    def _split(self,node):
        # if the node is root, the parent is a new node
        if node == self.root:
            p = TreeNode([None,None,None])
            self.root = p
        else:
            p = node.p

        # create left and right nodes and assign values and parents
        n1 = TreeNode([node.r[0],None,None])
        n2 = TreeNode([node.r[2],None,None])
        
        n1.r[0] = node.r[0]
        n2.r[0] = node.r[2]
        n1.p, n2.p = p,p
        
        if p.hasNoRoot():
            p.c = [n1,None,None,n2]
        #Split based on which pointer node was in
        #Left in 0, Middle in 1, Right in 3 
        else:
            index = p.c.index(node)
            if index == 0:
                if p.c[1] == None:
                    p.c = [n1,n2,None,p.c[3]]
                else:
                    p.c = [n1,n2,p.c[1],p.c[3]]
            elif index == 3 :
                if p.c[1] == None:
                    p.c = [p.c[0],n1,None,n2]
                else:
                    p.c = [p.c[0],p.c[1],n1,n2]
            else:
                p.c = [p.c[0],n1,n2,p.c[3]]
   
        
       #If it isnt a leaf, divy up the children
        if not node.isLeaf():
            n1.c = [node.c[0],None,None,node.c[1]]
            n2.c = [node.c[2],None,None,node.c[3]]
            for i in n1.c:
                if i != None:
                    i.p = n1
            for i in n2.c:
                if i != None:
                    i.p = n2

        
        #Move the middle up
        if p.hasNoRoot():
            p.r[0] = node.r[1]
            
        elif p.hasOneRoot():
            if node.r[1] <= p.r[0]:
                p.r = [node.r[1],p.r[0],None]
            else:
                p.r = [p.r[0],node.r[1],None]
        else:
            if node.r[1] <= p.r[0]:
                p.r = [node.r[1],p.r[0],p.r[1]]
            elif node.r[1] > p.r[0] and node.r[1] <= p.r[1]:
                p.r = [p.r[0],node.r[1],p.r[1]]
            else:
                p.r = [p.r[0],p.r[1],node.r[1]]
        
        #If the parent is still 3 split again
        if p.hasThreeRoot():
            self._split(p)



    """
    pre: value whos position is to be found
    post: returns the leaf the value should be inserted to
    """
    def _find(self,value):
        pointer = self.root
        while not pointer.isLeaf():
            if pointer.hasTwoRoot():
                if value < pointer.r[0]:
                    pointer = pointer.c[0]   #Less than
                elif value > pointer.r[1]:
                    pointer = pointer.c[3]   #Greater than
                else:
                    pointer = pointer.c[1]   #Middle

            else:
                if value <= pointer.r[0]:
                    pointer = pointer.c[0]   #Less than
                else:
                    pointer = pointer.c[3]   #Greater than
        return pointer


    """
    pre: value to be searched for
    post: True if found, False if not
    """
    def search(self,value):
        pointer = self.root
        while pointer != None:
            if value in pointer.r:
                return True
            else:
                if pointer.hasTwoRoot():
                    if value < pointer.r[0]:
                        pointer = pointer.c[0]   #Less than
                    elif value > pointer.r[1]:
                        pointer = pointer.c[3]   #Greater than
                    else:
                        pointer = pointer.c[1]   #Middle

                else:
                    if value <= pointer.r[0]:
                        pointer = pointer.c[0]   #Less than
                    else:
                        pointer = pointer.c[3]   #Greater than

        return False

    """
    post: returns the maximum depth of the tree
    """
    def maxdepth(self):
        return self._maxdepth(self.root)

    def _maxdepth(self,node):
        if node.isLeaf():
            return 0
        elif node.hasOneRoot():
            return max(self._maxdepth(node.c[0]) , self._maxdepth(node.c[3])) + 1
        else:
            return max(self._maxdepth(node.c[0]) , self._maxdepth(node.c[1]), self._maxdepth(node.c[3])) + 1

    """
    post: returns the average depth
    """
    def avgdepth(self):
        depths = []
        depth = -1
        self._avgdepth(self.root,depths,depth)
        return sum(depths) / len(depths)
        
        
    def _avgdepth(self,node,depths,depth):
        depth += 1
        depths.append(depth)
        if node.isLeaf():
            return
        elif node.hasOneRoot():
            self._avgdepth(node.c[0],depths,depth)
            self._avgdepth(node.c[3],depths,depth)
        else:
            self._avgdepth(node.c[0],depths,depth)
            self._avgdepth(node.c[1],depths,depth)
            self._avgdepth(node.c[3],depths,depth)  
            
    def size(self):
        return self.treesize

    """
    post: returns the inorder traversal of the tree
    """
    def inorder(self):
        lst = []
        self._inorder(self.root,lst)
        return lst
        
    def _inorder(self,node,lst):
        if node == None:
            return
        if node.isLeaf():
            lst.append(node.r[0])
            if node.hasTwoRoot():
                lst.append(node.r[1])
        elif node.hasOneRoot():
            self._inorder(node.c[0],lst)
            lst.append(node.r[0])
            self._inorder(node.c[3],lst)
        else:
            self._inorder(node.c[0],lst)
            lst.append(node.r[0])
            self._inorder(node.c[1],lst)
            lst.append(node.r[1])
            self._inorder(node.c[3],lst)



##a = list(range(10,3,-1))
##print(a)
##
##tree = TwoThreeTree()
##for e in a:
##    tree.insert(e)
##    print(tree.inorder())
##
##print("Max depth", tree.maxdepth())
##print("Avg depth", tree.avgdepth())
##
