package com.replay.timeclock.dialogs;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;

public class OptionsDialog extends DialogFragment {
    public interface OptionsDialogListener {
        void edit(Parcelable p);
        void delete(Parcelable p);
    }

    private Parcelable parcelable;
    private OptionsDialogListener optionsDialogListener;

    public static OptionsDialog newInstance(Parcelable p) {
        OptionsDialog e = new OptionsDialog();
        Bundle args = new Bundle();
        args.putParcelable("parcelable", p);
        e.setArguments(args);
        return e;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parcelable = getArguments().getParcelable("parcelable");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Options")
                .setItems(new CharSequence[]{"Edit","Delete"}, new DialogInterface.OnClickListener() {
                    final int edit = 0, delete = 1;
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case edit:
                                optionsDialogListener.edit(parcelable);
                                break;
                            case delete:
                                optionsDialogListener.delete(parcelable);
                                break;
                            default:
                                break;
                        }
                    }
                });

        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.optionsDialogListener = (OptionsDialogListener) activity;
        } catch (final ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement an OnCompleteListener");
        }
    }
}