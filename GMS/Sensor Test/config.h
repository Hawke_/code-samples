/*
 * config.h
 *
 * Created: 1/31/2018 4:42:25 PM
 *  Author: Nathan
 */ 


#ifndef CONFIG_H_
#define CONFIG_H_

#include <stdlib.h>
#include <avr/io.h>    
#include <avr/interrupt.h>
#include <util/delay.h>

#include "dbg/dbg_putchar.h"
#include "lindrv/lin_drv.h"           


#endif /* CONFIG_H_ */