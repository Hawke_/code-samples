# mips.py
# Nathan Frana
# Stage 4
# CS 360 - Winter 2017

from boolarray import BoolArray
from combinationalcircuits import CombinationalCircuits as CC
from sequentialcircuits import Memory,RegisterFile

class MIPS:
    def __init__(self):
        # Create and initalize the various permanent components
        # of the system diagram. {BoolArrays and Sequential circuit objects}
        # recall: The combinational components are simulated by functions
        #         not objects and can not be made into instance variables

        self.memory = Memory()
        self.register = RegisterFile()
        self.pc = BoolArray('0',32)
        self.instruction = BoolArray('0',32)
        
        
        self.opcode = BoolArray('0',4)

        self.rdreg1 = BoolArray('0',32)
        self.rdreg2 = BoolArray('0',32)
        self.wrtreg = BoolArray('0',32)
        self.rddata1 = BoolArray('0',32)
        self.rddata2 = BoolArray('0',32)
        self.immediate = BoolArray('0',11)

        self.alucontrol = BoolArray('0', 4)
        
    def IFcycle(self):

    #Fetch Instuction
        self.instruction = self.memory.instrCycle(self.pc)
        self.pcplus4 = CC.Add4(self.pc)
        print('---------- Instruction Fetch ----------')
        print('PC: ' + self.pc.toHex())
        print('PC+4: ' + self.pcplus4.toHex())
        print('Inst: ' + self.instruction.toHex())
        
    def IDcycle(self):
        print('---------- Instuction Decode ----------')

    # Control Signals
        (self.regdist, self.jump, self.branch, self.memread, self.memtoreg,
         self.aluop1, self.aluop0, self.memwrite, self.alusrc, self.regwrite) = CC.Control(self.instruction.subArray(26,32))

    # Instruction Jump
        self.jumpAddr = CC.shiftLeft2(self.instruction.subArray(0,25)).catHigh(self.pcplus4)
        
        print("RegDst:", self.regdist)
        print("Jump:", self.jump)
        print("Branch:", self.branch)
        print("MemRead:", self.memread)
        print("MemToReg:", self.memtoreg)
        print("ALUOP:", self.aluop1, self.aluop0)
        print("MemWrite:", self.memwrite)
        print("ALUSrc:", self.alusrc)
        print("RegWrite:", self.regwrite)

        print("Jump Addr:", self.jumpAddr.toHex())
        
        
    def RRcycle(self):
        print('---------- Register Read ----------')
        i = self.instruction
        rr1 = i.subArray(21,26)
        rr2 = i.subArray(16,21)
        rr3 = i.subArray(11,16)

        
        self.writeReg = CC.mux2(rr2, rr3, self.regdist)
        
        self.rData1, self.rData2 = self.register.readCycle(int(rr1),int(rr2),int(self.writeReg))

        self.immediate = i.subArray(0,16)
        
        print("RdReg1", rr1.toHex())
        print("RdReg2", rr2.toHex())
        print("WrtReg", self.writeReg.toHex())

        print("RdData1", self.rData1.toHex())
        print("RdData2", self.rData2.toHex())
        
        
                            
    def EXcycle(self):
        print('---------- Execute Hardware ----------')
        
        self.opcode = CC.ALUControl(self.aluop1, self.aluop0, self.instruction.subArray(0,6))

        extImmediate = CC.signExtend(self.immediate)
        ALU2 = CC.mux2(self.rData2, extImmediate, self.alusrc)

        self.ALUOut, zero = CC.ALU32(self.rData1, ALU2, self.opcode)

        branchAdd = CC.Add(self.pcplus4, CC.shiftLeft2(extImmediate))

        mux1 = CC.mux2(self.pcplus4, branchAdd, self.branch and zero)
        mux2 = CC.mux2(mux1, self.jumpAddr, self.jump)

        self.pc = mux2

        print("OpCode:", self.opcode.toHex())
        print("ALU Data 1:", self.rData1.toHex())
        print("ALU Data 2:", ALU2.toHex())
        print("ALU Result:", self.ALUOut.toHex())
        print("Zero:", zero)

        print("Branch Add:", branchAdd.toHex())
        print("Mux br,pc4:", mux1.toHex())
        print("New pc:", self.pc.toHex())

    def MMcycle(self):
        print('---------- Memory Access ----------')
        
        self.rdData = self.memory.dataCycle(self.ALUOut, self.rData2, self.memread, self.memwrite)
        
        print("Mem Addr:", self.ALUOut.toHex())
        print("Write Data:", self.rData2.toHex())
        print("RD Data:", self.rdData.toHex())
        

    def WBcycle(self):
        print('---------- Write Back ----------')
        mux = CC.mux2(self.ALUOut, self.rdData, self.memtoreg)

        self.register.writeCycle(self.regwrite, mux)

        print("Data:", mux.toHex())

    def IOcycle(self):
        """
        if inflag != 0
            prompt for input in hex
            store at inbuffer
            reset inflag
        if outflag != 0
            print buffer to screen
            reset outflag
        """
        
        print('---------- IO Cycle ----------')
        inflag = self.memory[1008]
        outflag = self.memory[1016]

        print("inflag", inflag.toHex())
        print("outflag", outflag.toHex())
        
        if int(inflag) != 0:
            self.memory.memory[1012] = BoolArray(str(input("Enter 8 digit Hex: ")),32)
            self.memory[1008] = BoolArray('0',32)

        if int(outflag) != 0:
            print()
            print("Output:",self.memory.memory[1020].toHex())
            self.memory[1016] = BoolArray('0',32)


        
    def run(self):
        output = input("Display Registers and Memory after each step? (Y/N)") == "Y"
        pause = input("Pause between instructions? (Y/N)") == "Y"
        while True:
            print()
            print()
            print()
            print("=======================================")
            print("          Instuction")
            print("=======================================")
            
            self.IFcycle()
            if self.instruction.toHex() == 'FC000000':
                return
            self.IDcycle()
            self.RRcycle()
            self.EXcycle()
            self.MMcycle()
            self.WBcycle()
            self.IOcycle()


            print()
            if output:
                self.showRegisters()
                self.showMemory()
            if pause:
                a = input("Press Enter to Continue")
            print()


    def showMemory(self):
        # call your Memory object's showMemory() method
        print()
        print()
        print('---------- Memory ----------')
        self.memory.showMemory()

    def showRegisters(self):
        # call your RegisterFile objects's showRegisters() method
        print()
        print()
        print('---------- Registers ----------')
        self.register.showRegisters()
    
    def loadProgram(self,fname):
        # call your Memory object's loadProgram() method
        self.memory.loadProgram(fname)




if __name__=="__main__":
    mips=MIPS()
    mips.loadProgram(input("What machine language file? "))
    mips.run()
    mips.showMemory()
    mips.showRegisters()

