package com.replay.timeclock;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.SystemClock;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ListView;
import android.widget.Toast;

import com.replay.timeclock.dbmanager.Entry;
import com.replay.timeclock.dbmanager.Job;
import com.replay.timeclock.dbmanager.SQLiteManager;
import com.replay.timeclock.dialogs.EntryEditDialog;
import com.replay.timeclock.dialogs.OptionsDialog;
import com.replay.timeclock.export.ExportManager;
import com.replay.timeclock.listview.AddElement;
import com.replay.timeclock.listview.EntryElement;
import com.replay.timeclock.listview.Item;
import com.replay.timeclock.listview.ListAdapter;
import com.replay.timeclock.listview.ListElement;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;


public class JobActivity extends Activity implements OptionsDialog.OptionsDialogListener, EntryEditDialog.EntryEditDialogListener {
    private ListView mView;
    private ListAdapter mAdapter;
    private Chronometer chronometer;
    private Button button;
    private boolean recording = false;
    private Calendar startTime;

    private SQLiteManager sqLiteManager;
    private Job job;
    private List<Entry> entries;
    private ArrayList<Item> items;

    NotificationManager notificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job);
        job = getIntent().getExtras().getParcelable("job");
        items = new ArrayList<Item>();

        sqLiteManager = SQLiteManager.getInstance(this);

        mView = (ListView) findViewById(R.id.job_listview);
        mAdapter = new ListAdapter(this,items);
        mView.setAdapter(mAdapter);

        mView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long l) {
                int type = mView.getAdapter().getItemViewType(position);
                if (type == ListAdapter.RowType.ENTRY_ELEMENT.ordinal()) {
                    // Open EntryEditDialog
                    int i = mAdapter.getItem(position).getID();
                    OptionsDialog optionsDialog = OptionsDialog.newInstance(sqLiteManager.getEntry(job,i));
                    optionsDialog.show(getFragmentManager(), "option_dialog");

                }
                return false;
            }
        });
        final TimeConverter timeConverter = new TimeConverter();

        chronometer = (Chronometer) findViewById(R.id.timer_element_chrono);
        button = (Button) findViewById(R.id.timer_element_button);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(recording) {
                    chronometer.stop();
                    button.setText("Start");
                    recording = false;
                    Calendar endTime = Calendar.getInstance();

                    Entry e = new Entry();
                    e.startTime = timeConverter.dateToString(startTime);
                    e.endTime = timeConverter.dateToString(endTime);
                    e.hours = timeConverter.differenceInDate(endTime,startTime);

                    sqLiteManager.addEntry(job,e);
                    updateJobList();

                } else {
                    chronometer.setBase(SystemClock.elapsedRealtime());
                    chronometer.start();
                    button.setText("Stop");
                    recording = true;
                    startTime = Calendar.getInstance();
                }
            }
        });

        updateJobList();
    }

    public void updateJobList() {
        entries = sqLiteManager.getAllEntries(job);

        items = new ArrayList<Item>();
        //items.add(new TimerElement(mAdapter));

        for(Entry e: entries){
            String start = e.startTime;
            String end = e.endTime;
            float hours = e.hours;
            items.add(new EntryElement(start,end,hours, e.id));
        }
        mAdapter.updateItems(items);
    }

    private void makeNotification(Context context) {
        Intent intent = new Intent(context, JobActivity.class);
        intent.putExtra("job", job);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 001, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentTitle("TimeClock");
        builder.setContentText("Entry Currently Ongoing");
        builder.setContentIntent(pendingIntent);
        builder.setSmallIcon(R.drawable.icon);
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.add));
        Notification n;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN ) {
            n = builder.build();
        } else {
            n = builder.getNotification();
        }

        n.flags = Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;

        notificationManager.notify(001,n);

    }
    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    public void edit(Parcelable p) {
        EntryEditDialog entryEditDialog = EntryEditDialog.newInstance(p);
        entryEditDialog.show(getFragmentManager(), "edit_entry_dialog");
    }

    @Override
    public void delete(Parcelable p) {
        Entry e = (Entry) p;
        sqLiteManager.deleteEntry(job,e);
        updateJobList();
        Toast.makeText(this, "Entry Deleted.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void edited(Parcelable p) {
        Entry e = (Entry) p;
        sqLiteManager.updateEntry(job,e);
        updateJobList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.export_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.export:
                ExportManager exportManager = new ExportManager();
                exportManager.ExportJobToCSV(job,sqLiteManager.getAllEntries(job));
                Toast.makeText(this, "Entries Exported", Toast.LENGTH_SHORT).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
