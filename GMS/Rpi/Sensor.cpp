#include "Sensor.h"

Sensor::Sensor() {
	status = NODETECT;
	id = 0x00;
}
Sensor::Sensor(uint8_t tid, int tstatus) {
	status = NODETECT;
	id = tid;
}

void Sensor::setStatus(int tstat) {
	status = tstat;
}

int Sensor::getStatus() {
	return status;
}

void Sensor::setID(uint8_t tid) {
	id = tid;
}
uint8_t Sensor::getID() {
	return id;
}







