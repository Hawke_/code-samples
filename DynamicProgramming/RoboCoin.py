#Nathan Frana - Robot Coin
import random
from graphics import *
    

class RoboCoin():
                                                #   0 1 2 3 4
    x = 0
    y = 0                                       # 0
    coins = 0                                   # 1
                                                # 2
    board = {}                                  # 3
    F = {}                                      # 4

    maxVal = 0
    path = []
    
    def __init__(self,c,r,coin):
        self.x = c
        self.y = r
        self.coins = coin

        #Set All spots as empty(False)
        for x in range(c):
            for y in range(r):
                self.board[(x,y)] = 0
                self.F[(x,y)] = 0
                
        #Place the coins(True) no repeats

        for i in range(coin):
            a = random.randrange(0,c)
            b = random.randrange(0,r)
            while self.board[(a,b)] == 1:
                a = random.randrange(0,c)
                b = random.randrange(0,r)
            self.board[(a,b)] = 1
        
        self._walk()
        self._render()


    def _walk(self):
        #n = rows m = cols
        self.F[(0,0)] = self.board[(0,0)]
        for j in range(1,self.y):
            self.F[(0,j)] = self.F[(0,j-1)] + self.board[(0,j)]
        
        for i in range(1,self.x):
            self.F[(i,0)] = self.F[(i-1,0)] + self.board[(i,0)]
            for j in range(1,self.y):
                self.F[(i,j)] = max(self.F[(i-1,j)],self.F[(i,j-1)]) + self.board[(i,j)]

        self.maxVal = self.F[(self.x-1,self.y-1)]
        
        path = []
        i = self.x-1
        j = self.y-1
        self.path.append((i,j))
        while i != 0 and j != 0:
            if self.F[(i,j-1)] > self.F[(i-1,j)]:
                self.path.append((i,j-1))
                j -= 1
            else:
                self.path.append((i-1,j))
                i -= 1
        self.path.append((0,0))
        


    def _render(self):
        
        size = 75
        offset = size // 2
        
        xbound = self.x*size
        ybound = self.y*size


        
        window = GraphWin("Robot Coin Search",xbound+100,ybound)
        for i in range(self.x):
            ln = Line(Point(i*size,0),Point(i*size,ybound))
            ln.draw(window)
        for i in range(self.y):
            ln = Line(Point(0,i*size),Point(xbound,i*size))
            ln.draw(window)

        for e in self.board.keys():
            if self.board[e] == 1:
                crl = Circle(Point(e[0]*size+offset,e[1]*size+offset),5)
                crl.setFill("black")
                crl.draw(window)
            txt = Text(Point(e[0]*size+offset+(size//2-10),e[1]*size+offset+(size//2 -10)),str(self.F[e]))
            txt.setSize(10)
            txt.setFill("red")
            txt.draw(window)

        for i in range(1,len(self.path)):
            ln = Line(Point(self.path[i-1][0] *size+offset,self.path[i-1][1]*size+offset),Point(self.path[i][0]*size+offset,self.path[i][1]*size+offset))
            ln.setWidth(3)
            ln.draw(window)
            
        text = Text(Point(xbound+50,ybound//2),"Max Coins: " + str(self.maxVal))
        text.draw(window)

        window.getMouse()
        window.close()


if __name__ == "__main__":
    a = RoboCoin(5,5,6)
##    b = RoboCoin(6,10,14)
##    c = RoboCoin(10,6,12)

        
        
