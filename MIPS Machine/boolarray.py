# boolarray.py
# Nathan Frana
# Stage One
# CS 360 - Winter 2017

class BoolArray:
    """ A BoolArray object has as its main instance variable a
    variable length array of boolean constants"""

    hexToBin = {'0':'0000', '1':'0001', '2':'0010', '3':'0011', '4':'0100',
                '5':'0101', '6':'0110', '7':'0111', '8':'1000', '9':'1001',
                'A':'1010', 'B':'1011', 'C':'1100', 'D':'1101', 'E':'1110',
                'F':'1111'}

    binToHex = {v: k for k, v in hexToBin.items()}
    

    def __init__(self,hexstr="00000000",n=32):
        """ Convert the hextstring to a length 'n' list of booleans
        be careful of the order of indexing. (see assigment)
        e.g., BoolArray("5C",8) should produce:
        [False, False, True,True,True,False,True,False]
        i: 0      1      2    3   4     5    6     7
        the hexstr will consistantly use upper case for hex digits
        that are alphabetic i.e., "A", "B","C", "D", "E"."""

        binstr = self._hexToBin(hexstr)       
        if len(binstr) > n:
            binstr = binstr[len(binstr)-n:]

        while len(binstr) < n:
            binstr = '0' + binstr

        self.data = []
        for n in binstr:
            if n == '1':
                self.data.append(True)
            else:
                self.data.append(False)
        self.data = self.data[::-1]


    def __str__(self):
        """ __str__ is called when you 'print' a BoolArray object or str() it.
        It should return a string of 0's and 1's in proper order.
        The example in the init, "5C" should return  "01011100" """

        return self._boolArrayToBin(self.data)

        
    def __len__(self):
        """ __len__ is called when you use the the built in len()
        it should return the number of bits from the __init__"""

        return len(self.data)


    def toHex(self):
        """ return a string representing the current value in base 16
        eg. a string whose current barray is [True,False,False,True,False]
        should return "09"  (not "12"). The number of hex digits should be
        len//4 rounded up. Use upper case for alpha digits,
        i.e., "A" not "a"."""

        binstr = str(self)
        return self._binToHex(binstr)
        

    def __int__(self):
        """ Returns an unsigned integer equivalent to the number represented
        the example "5C" should return 92"""

        binstr = str(self)
        return self._binToInt(binstr)


    def subArray(self,left,rightplus):
        """ Extracts a subarray of bits from indices left through rightplus
        including left and upto but not including rightplus, python style.
        e.g.  subArray(7,10)  returns a BoolArray object with bits equal to
        bits 7 through 9 inclusive."""

        binstr = self._boolArrayToBin(self.data[left:rightplus])
        if len(binstr) % 4 != 0:                                #Keep it as multiples of 4
            binstr = "0"*(4 - len(binstr) % 4) + binstr

        
        return BoolArray(self._binToHex(binstr), rightplus-left)        


    def __setitem__(self,i,boolval):
        """ Sets the ith bit of the array to the value boolval.
        It is called using indexing:  PC[i] = boolval
        where PC is a BoolArray object."""

        self.data[i] = boolval


    def __getitem__(self,i):
        """ Returns the ith bit of the array
        It is called using [] indexing:   t = PC[i]
        where PC is a BoolArray object"""

        return self.data[i]
        

    def catHigh(self,barrayobj):
        """ Returns a new BoolArray Object whose bits are those
        of self with bits of barray concatenated on the high end.
        The length of the new object should not be allowed to exceed 32."""

        bArrayA = self.data
        bArrayB = barrayobj.data

        bArray = bArrayA + bArrayB
        binstr = self._boolArrayToBin(bArray)
        hexstr = self._binToHex(binstr)
        
        return BoolArray(hexstr, min(len(hexstr)*4 ,32))


    def _hexToBin(self,hexstr):
        """ Returns a binary representation of a hex string, order high to low"""

        binstr = ''
        for char in hexstr:
            binstr += self.hexToBin[char]

        return binstr


    def _binToInt(self,binstr):
        """Returns an integer representing a binary string"""

        result = 0
        for val in binstr:
            result *= 2
            result += int(val)

        return result


    def _binToHex(self,binstr):
        """ Returns a hex representation of a binary string"""

        if len(binstr) % 4 != 0:                                #Must be 0 (mod 4)
            binstr = "0"*(4 - len(binstr) % 4) + binstr
        
        hexstr = ''
        for i in range(0, len(binstr), 4):
            hexstr += self.binToHex[binstr[i:i+4]]

        return hexstr


    def _boolArrayToBin(self, boolArray):
        """returns a binary representation of a boolArray, order high to low"""

        binstr = ''
        for val in boolArray:
            if val == True:
                binstr += "1"
            else:
                binstr += "0"
        return binstr[::-1]

    def copy(self):
        copy = BoolArray('0', len(self))
        for i in range(len(self.data)):
            copy.data[i] = self.data[i]

        return copy

        

