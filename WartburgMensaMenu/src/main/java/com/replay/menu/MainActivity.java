
/*
 * Nathan Frana
 * Modified: 1/23/16 7:39 PM.
 */

package com.replay.menu;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    WebView webview;
    WebSettings settings;
    String month, day, year;
    String url;
    Calendar c;

    Boolean connection = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_PROGRESS);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);


        webview = (WebView) findViewById(R.id.webView);
        settings = webview.getSettings();
        c = Calendar.getInstance();


        if (android.os.Build.VERSION.SDK_INT >= 19) {
            settings.setUseWideViewPort(true);
            settings.setLoadWithOverviewMode(true);
        }

        final Activity activity = this;
        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                // Activities and WebViews measure progress with different scales.
                // The progress meter will automatically disappear when we reach 100%
                activity.setProgress(progress * 1000);
            }
        });

        webview.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                webview.loadUrl("file:///android_asset/noconnection.html");
                connection = false;


            }
        });

        webview.setOnTouchListener(new OnSwipeTouchListener(this) {

            @Override
            public void onSwipeLeft() {
                c.add(Calendar.DAY_OF_YEAR, 1);
                setDate();
                webview.loadUrl(url);
            }

            @Override
            public void onSwipeRight() {
                c.add(Calendar.DAY_OF_YEAR, -1);
                setDate();
                webview.loadUrl(url);
            }

            @Override
            public void onTapDown() {
                if (connection == false) {
                    connection = true;
                    Toast.makeText(activity, "Refreshing", Toast.LENGTH_SHORT).show();
                    webview.loadUrl(url);
                }
            }
        });


        setDate();
        webview.loadUrl(url);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_previous) {
            c.add(Calendar.DAY_OF_YEAR, -1);
            setDate();
            webview.loadUrl(url);

        }
        if (id == R.id.action_next) {
            c.add(Calendar.DAY_OF_YEAR, 1);
            setDate();
            webview.loadUrl(url);

        }

        if (id == R.id.action_get) {
            try {
                PackageManager mPm = getPackageManager();  // 1
                PackageInfo info = mPm.getPackageInfo("com.cbord.get", 0);  // 2,3

                Intent intent = mPm.getLaunchIntentForPackage("com.cbord.get");
                startActivity(intent);

            } catch (PackageManager.NameNotFoundException e) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://get.cbord.com/wartburg/full/login.php"));
                startActivity(browserIntent);
            }
        }
        return super.onOptionsItemSelected(item);
    }


    public void setDate() {
        day = Integer.toString(c.get(Calendar.DAY_OF_MONTH));
        month = Integer.toString(c.get(Calendar.MONTH) + 1);
        year = Integer.toString((c.get(Calendar.YEAR)));
        url = "http://public.wartburg.edu/diningmenu/DiningHall/daily.asp?1=" + month + "%2F" + day + "%2F" + year;
    }
}
