#include "lin_controller.h"

#define TX_PIN	20
#define RX_PIN	21
#define SE_PIN	26
#define LIN_BIT_WAIT 52

void init_controller(void) {
	wiringPiSetupSys();
	pinMode(SE_PIN, OUTPUT);
	pinMode(TX_PIN, OUTPUT);
	pinMode(RX_PIN, INPUT);
	pullUpDnControl(RX_PIN, PUD_UP);

	digitalWrite(SE_PIN, HIGH);
	digitalWrite(TX_PIN, HIGH);
}

void lin_transmit_byte(uint8_t byte) {
	digitalWrite(TX_PIN, LOW);
	delayMicroseconds(LIN_BIT_WAIT);

	for (int i = 0; i < 8; i++) {
		digitalWrite(TX_PIN, (byte & (1 << i)) ? HIGH : LOW);
		delayMicroseconds(LIN_BIT_WAIT);
	}
	digitalWrite(TX_PIN, HIGH);
	delayMicroseconds(LIN_BIT_WAIT);
}

uint8_t lin_receive_byte(void) {
	uint8_t result = 0x00;

	int timeout = micros();

	while (digitalRead(RX_PIN) && (micros() - timeout < (LIN_BIT_WAIT * 20))) {}
	/*
	if ((micros() - timeout) > (LIN_BIT_WAIT * 20)) {
	return result;
	}
	*/
	delayMicroseconds(LIN_BIT_WAIT + 3);

	for (int i = 0; i < 8; i++) {
		result |= (digitalRead(RX_PIN) << i);
		delayMicroseconds(LIN_BIT_WAIT);
	}

	delayMicroseconds(LIN_BIT_WAIT - 3);

	//Wait for Checksum to pass
	for (int i = 0; i < 10; i++) {
		delayMicroseconds(LIN_BIT_WAIT);
	}

	return result;
}

uint8_t lin_exchange(uint8_t id) {
	uint8_t parity0 = ((id >> 0) & 1) ^ ((id >> 1) & 1) ^ ((id >> 2) & 1) ^ ((id >> 4) & 1);
	uint8_t parity1 = !( ((id >> 1) & 1) ^ ((id >> 3) & 1) ^ ((id >> 4) & 1) ^ ((id >> 5) & 1));
	uint8_t calc_id = id | (parity0 << 6) | (parity1 << 7);

	static pthread_mutex_t cs_mutex = PTHREAD_ADAPTIVE_MUTEX_INITIALIZER_NP;
	pthread_mutex_lock(&cs_mutex);

	digitalWrite(TX_PIN, LOW);
	delayMicroseconds(LIN_BIT_WAIT * 14);
	digitalWrite(TX_PIN, HIGH);
	delayMicroseconds(LIN_BIT_WAIT);

	lin_transmit_byte(0x55);
	lin_transmit_byte(calc_id);

	uint8_t resp = lin_receive_byte();
	pthread_mutex_unlock(&cs_mutex);

	return resp;
}