
#define NODETECT	0
#define DETECT		1

#include <stdint.h>
class Sensor
{
	int status;
	uint8_t id;
public:
	Sensor();
	Sensor(uint8_t id, int status);
	int getStatus();
	void setStatus(int status);
	void setID(uint8_t tid);
	uint8_t getID();

};

