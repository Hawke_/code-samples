#Nathan Frana - Coin Row

class CoinRow():
    C = []
    optimumSet = []
    optimumInd = []
    maxVal = 0
    

    def __init__(self,v):
        self.C = v
        self._walk()
    

    def maxValue(self):
        return self.maxVal

    def optSet(self):
        return self.optimumSet

    def optInd(self):
        return self.optimumInd
    
    def _walk(self):

        # Walk down the list
        F = list(range(len(self.C)+1))
        F[0],F[1] = 0,self.C[0]
        for i in range(2,len(F)):
            F[i] = max(self.C[i-1] + F[i-2], F[i-1])
        self.maxVal = F[-1]

        #Reverse back up the list
        i = len(F)-1
        while i > 0:
            a = self.C[i-1] + F[i-2]
            b = F[i-1]

            if a > b:
                self.optimumSet.append(self.C[i-1])
                self.optimumInd.append(i-1)
                i -=2
            else:
                i -=1
        #Put it left to right from right to left
        self.optimumSet.reverse()
        self.optimumInd.reverse()
        
 



##
##A = CoinRow([5,1,2,10,6,2])
##print("17 should be")
##print(A.maxValue())
##print("[5, 10, 2] should be")
##print(A.optSet())
##
