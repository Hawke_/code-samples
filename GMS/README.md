# Grain Level Monitoring System #

My senior project is a cheap, easy to use, and modular system of
sensors that can be attached to any bin. The main idea is to make it easier for farmers to
see their grain levels without having to climb the bin. This is accomplished by setting sensors
at predetermined heights with an accompanying LED light on the outside at the same level. 
All sensors are daisy chained together and communicating to a main controller outside of the bin
at ground level using a LIN bus. The controller (raspberry Pi) is also able to display volume metrics
and to allow for customized settings. 