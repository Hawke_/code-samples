package com.replay.timeclock.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.replay.timeclock.R;


public class JobDialog extends DialogFragment {

    public interface JobDialogListener{
        void positivePressed(String title, float wage);
    }

    private JobDialogListener jobDialogListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.job_dialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.setTitle("Add Job");
        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                EditText name = (EditText) view.findViewById(R.id.job_dialog_text);
                EditText wage = (EditText) view.findViewById(R.id.job_dialog_wage);
                jobDialogListener.positivePressed(name.getText().toString(),
                        Float.valueOf(wage.getText().toString()));
                getDialog().dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismiss();
            }
        });

        return builder.create();


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.jobDialogListener = (JobDialogListener)activity;
        } catch (final ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnCompleteListener");
        }
    }
}
