#Nathan Frana
#Project 4
#Driver class for 2-3 Tree implementation

import random
from TwoThreeTree import TwoThreeTree

def main():
    print("This is an implementation for a 2-3 tree.")
    n = int(input("Please enter a number of elements you would like in the tree: "))
    n = random.sample(range(1,n+1),n) #n random numbers ranging from 1-n+1

    tree = TwoThreeTree(n)

    print()
    print("The size of the tree is ", tree.size())
    print("The maximum depth is ", tree.maxdepth())
    print("The average depth is ", tree.avgdepth())
    print()
    print("Would you like to see the inorder traversal of the tree?")
    if str(input("(Y/N)?: ")).upper() == "Y":
        print(tree.inorder())



if __name__ == "__main__":
    main()
