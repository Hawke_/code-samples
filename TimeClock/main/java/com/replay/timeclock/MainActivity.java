package com.replay.timeclock;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;


import com.replay.timeclock.dbmanager.Entry;
import com.replay.timeclock.dbmanager.Job;
import com.replay.timeclock.dbmanager.SQLiteManager;
import com.replay.timeclock.dialogs.EditDialog;
import com.replay.timeclock.dialogs.JobDialog;
import com.replay.timeclock.dialogs.OptionsDialog;
import com.replay.timeclock.export.ExportManager;
import com.replay.timeclock.listview.AddElement;
import com.replay.timeclock.listview.Item;
import com.replay.timeclock.listview.ListAdapter;
import com.replay.timeclock.listview.ListElement;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements JobDialog.JobDialogListener, OptionsDialog.OptionsDialogListener, EditDialog.EditDialogListener {
    private ListView mView;
    private ListAdapter mAdapter;
    private Context context;

    private SQLiteManager sqLiteManager;
    private List<Job> jobs;
    private ArrayList<Item> items;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        items = new ArrayList<Item>();
        //context.deleteDatabase("TimeClock.db");

        sqLiteManager = SQLiteManager.getInstance(this);

        mView = (ListView) findViewById(R.id.main_listview);
        mAdapter = new ListAdapter(this, items);
        mView.setAdapter(mAdapter);

        mView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int type = mView.getAdapter().getItemViewType(position);
                if (type == ListAdapter.RowType.ADD_ELEMENT.ordinal()) {
                    JobDialog jobDialog = new JobDialog();
                    jobDialog.show(getFragmentManager(), "job_dialog");
                }
                if (type == ListAdapter.RowType.LIST_ELEMENT.ordinal()) {
                    int i = mAdapter.getItem(position).getID();
                    Job job = sqLiteManager.getJob(i);
                    System.out.println(job.id);
                    Intent intent = new Intent(MainActivity.this, JobActivity.class);
                    intent.putExtra("job", job);
                    context.startActivity(intent);
                }
            }
        });

        mView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                int type = mView.getAdapter().getItemViewType(position);
                if (type == ListAdapter.RowType.LIST_ELEMENT.ordinal()) {
                    int i = mAdapter.getItem(position).getID();
                    if (i != -1) {
                        OptionsDialog optionsDialog = OptionsDialog.newInstance(sqLiteManager.getJob(i));
                        optionsDialog.show(getFragmentManager(), "option_dialog");
                        return true;
                    }
                }
                return false;
            }
        });

        updateJobList();
    }

    @Override
    protected void onResume() {
        updateJobList();
        super.onResume();
    }

    public void updateJobList() {
        jobs = sqLiteManager.getAllJobs();

        items = new ArrayList<Item>();
        items.add(new AddElement("Add Job"));
        for (Job j : jobs) {
            updateJobHours(j);
            items.add(new ListElement(j.name, j.hours + " hrs. | $" + j.wage*j.hours, j.id));

        }
        mAdapter.updateItems(items);
    }

    public void updateJobHours(Job job) {
        List<Entry> entries = sqLiteManager.getAllEntries(job);
        float hours = 0.0f;
        for (Entry e : entries) {
            hours += e.hours;
        }
        job.hours = hours;
        sqLiteManager.updateJob(job);
    }

    @Override
    public void positivePressed(String title, float wage) {
        Job job = new Job(title, 0.0f, wage);
        sqLiteManager.addJob(job);
        updateJobList();
    }

    @Override
    public void edit(Parcelable p) {
        EditDialog editDialog = EditDialog.newInstance(p);
        editDialog.show(getFragmentManager(), "edit_dialog");
    }

    @Override
    public void delete(Parcelable p) {
        Job j = (Job) p;
        sqLiteManager.deleteJob(j);
        updateJobList();
        Toast.makeText(context, "Job Deleted.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void edited(Parcelable p) {
        Job j = (Job) p;
        sqLiteManager.updateJob(j);
        updateJobList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.export_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.export:
                ExportManager exportManager = new ExportManager();
                exportManager.ExportJobsToCSV(sqLiteManager.getAllJobs());
                Toast.makeText(context, "Job Exported", Toast.LENGTH_SHORT).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
