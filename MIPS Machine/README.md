# Basic MIPS Machine #

This semester-long project is designed to simulate the hardware of a basic 32-bit MIPS Machine using python. Harware and circuitry is emulated entirely using boolean logic. Running mips.py enables the ability to run 8-bit hex instructions defined by the MIPS instruction set.  