package com.replay.timeclock.dbmanager;


import android.os.Parcel;
import android.os.Parcelable;

public class Entry implements Parcelable {
    public int id;
    public String startTime;
    public String endTime;
    public float hours;

    public Entry(){

    }

    public Entry(int id, String startTime, String endTime, float hours){
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
        this.hours = hours;
    }


    protected Entry(Parcel in) {
        id = in.readInt();
        startTime = in.readString();
        endTime = in.readString();
        hours = in.readFloat();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(startTime);
        dest.writeString(endTime);
        dest.writeFloat(hours);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Entry> CREATOR = new Parcelable.Creator<Entry>() {
        @Override
        public Entry createFromParcel(Parcel in) {
            return new Entry(in);
        }

        @Override
        public Entry[] newArray(int size) {
            return new Entry[size];
        }
    };
}
