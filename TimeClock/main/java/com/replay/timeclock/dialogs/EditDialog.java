package com.replay.timeclock.dialogs;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;;

import com.replay.timeclock.R;
import com.replay.timeclock.dbmanager.Job;

public class EditDialog extends DialogFragment {
    public interface EditDialogListener {
        void edited(Parcelable p);
    }

    private Parcelable parcelable;
    private EditDialogListener editDialogListener;

    public static EditDialog newInstance(Parcelable p) {
        EditDialog e = new EditDialog();
        Bundle args = new Bundle();
        args.putParcelable("parcelable", p);
        e.setArguments(args);
        return e;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parcelable = getArguments().getParcelable("parcelable");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.job_dialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);

        final EditText name = (EditText) view.findViewById(R.id.job_dialog_text);
        final EditText wage = (EditText) view.findViewById(R.id.job_dialog_wage);
        final Job j = (Job) parcelable;
        name.setText(j.name);
        wage.setText(String.valueOf(j.wage));

        builder.setTitle("Edit Job");
        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                j.name = name.getText().toString();
                j.wage = Float.valueOf(wage.getText().toString());
                editDialogListener.edited(j);
                dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismiss();
            }
        });

        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.editDialogListener = (EditDialogListener) activity;
        } catch (final ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnCompleteListener");
        }
    }
}
