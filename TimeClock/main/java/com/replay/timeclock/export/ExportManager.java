package com.replay.timeclock.export;

import android.os.Environment;
import android.os.SystemClock;
import android.provider.Settings;

import com.opencsv.CSVWriter;
import com.replay.timeclock.dbmanager.Entry;
import com.replay.timeclock.dbmanager.Job;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;


public class ExportManager {



    public ExportManager() {

    }

    public void ExportJobsToCSV(List<Job> jobs) {
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File file = new File(dir, "Jobs.csv");

        try (FileWriter fileWriter = new FileWriter(file)) {
            if(!file.exists()){
                file.createNewFile();
            }
            fileWriter.append("Name, Hours, Wage\n");
            for(Job j: jobs) {
                String s = j.name + "," + j.hours + "," + j.wage + "\n";
                fileWriter.append(s);
            }
            fileWriter.close();
            System.out.println("Exported Finished.");
        } catch (IOException e) {
            System.out.println("Exort Aborted, " + e.toString());
        }
    }

    public void ExportJobToCSV(Job job, List<Entry> entries) {
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File file = new File(dir, job.name + ".csv");

        try (FileWriter fileWriter = new FileWriter(file)) {
            if(!file.exists()){
                file.createNewFile();
            }
            fileWriter.append("Start Time, End Time, Hours\n");
            for(Entry e: entries) {
                String s = e.startTime + "," + e.endTime + "," + e.hours + "\n";
                fileWriter.append(s);
            }
            fileWriter.close();
        } catch (IOException e) {

        }
    }



}
