package com.replay.timeclock.listview;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.replay.timeclock.R;


public class ListElement implements Item {
    private final String mainText, subText;
    private final int id;

    public ListElement(String main, String sec, int id) {
        this.mainText = main;
        this.subText = sec;
        this.id = id;
    }

    @Override
    public int getViewType() {
        return ListAdapter.RowType.LIST_ELEMENT.ordinal();
    }

    @Override
    public View getView(LayoutInflater inflater, View convertView) {
        View view;
        if (convertView == null) {
            view = inflater.inflate(R.layout.list_element,null);
        } else {
            view = convertView;
        }
        TextView mText = (TextView) view.findViewById(R.id.list_element_main_text);
        TextView sText = (TextView) view.findViewById(R.id.list_element_sub_text);

        mText.setText(mainText);
        sText.setText(subText);

        return view;
    }

    @Override
    public int getID(){
        return id;
    }
}
