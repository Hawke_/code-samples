package com.replay.timeclock.listview;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.replay.timeclock.R;


public class AddElement implements Item {

    private final String mText;

    public AddElement(String text) {
        this.mText = text;
    }

    @Override
    public int getViewType() {
        return ListAdapter.RowType.ADD_ELEMENT.ordinal();
    }

    @Override
    public View getView(LayoutInflater inflater, View convertView) {
        View view;
        if (convertView == null) {
            view = inflater.inflate(R.layout.add_element,null);
        } else {
            view = convertView;
        }

        TextView text = (TextView) view.findViewById(R.id.add_element_text);
        text.setText(mText);

        return view;
    }

    public int getID(){
        return -1;
    }
}
