# Nathan Frana - Project 5
# Driver for Coin Row and Robot Coin Path

from CoinRow import CoinRow
from RoboCoin import RoboCoin

def main():
    print("This is an implementation of Coin Row and Robot Coin Path\n")
    val = -1
    while val != 0:
        print("Please Choose and Option")
        print(" 1. Coin Row \n 2. Robot Coin Path \n 0. Quit")
        val = int(input())
        print()
        if val == 1:
            print("Enter a list of coins, ex 1,2,3,4")
            coins = input().split(",")
            coins = [int(i) for i in coins]
            row = CoinRow(coins)
            print()
            print("The maximum value is " + str(row.maxValue()))
            print("The coins used are " + str(row.optSet()) +
                  " at indicies " + str(row.optInd()))
        elif val == 2:
            print("How many colums would you like?")
            col = int(input())
            print("How many rows would you like?")
            row = int(input())
            print("How many coins would you like?")
            coins = int(input())

            RoboCoin(col,row,coins)
            
            print()
            
        elif val == 0:
            break
        else:
            print("Invalid input\n")



if __name__ == "__main__":
    main()
