# combinationalcircuits.py
# Nathan Frana
# Stage 2
# CS 360 - Winter 2017

from boolarray import BoolArray

class CombinationalCircuits:
   
   def mux2(In0,In1,c):
      """ Two-way Multiplexor
      #####################
      # pre: In0 and In1 are BoolArray objects of equal length
      #      and c is Boolean
      # If c is False it returns a copy of In0.
      # If c is True  it returns a copy of In1.
      """
      ans = BoolArray('0',len(In0))
      for i in range(len(In0)):
         a,b  = bool(In0[i]), bool(In1[i]) 
         ans[i] = (c and b) or (not c and a)

      return ans


   def mux4(In0,In1,In2,In3,s1,s0):
      """ Four way multiplexor
      ############################
      # pre: In0-In3 are BoolArrays, s0,s1 are bools
      """
      ans = BoolArray('0',len(In0))

      for i in range(len(In0)):
         a = In0.subArray(i,i+1)
         b = In1.subArray(i,i+1)
         c = In2.subArray(i,i+1)
         d = In3.subArray(i,i+1)
         
         mux1 = CombinationalCircuits.mux2(a,b,s0)
         mux2 = CombinationalCircuits.mux2(c,d,s0)
         
         ans[i] = CombinationalCircuits.mux2(mux1,mux2,s1)[0]
       
      return ans

        
   def signExtend(Ain):
      """ 16 to 32 bit sign extender
      ############################
      # pre: Ain is a BoolArray of length 16
      # Return a BoolArray of length 32 that is a
      # sign extention of Ain. i.e.,
      #  Aout[i] = Ain[i]  for i = 0..15
      #  Aout[i] = Ain[15] for i = 16..31
      """
      c = bool(Ain[-1])
      a = BoolArray('0', 32)
      for i in range(0,16,1):
         a[i] = Ain[i]
      for i in range(16,32,1):
         a[i] = c

      return a


   def shiftLeft2(Ain):
      """ Logical Left Shift by 2
      #########################
      # pre: Ain is a BoolArray (of length m)
      # return a BoolArray which is shifted left 2, filled with [False,False]
      # and whose length is min(32,m+2)
      """
      length = min(32,len(Ain)+2)
      ans = BoolArray('0', length)
      for i in range(0,length-2):
         ans[i+2] = Ain[i]

      return ans


   def Control(opcode):
      """ Control unit
      ##############
      # pre: opcode is a BoolArray of length 6 and has a (hex) value
      #      of one of: 00,02,04,08,23, or 2B
      # Returns 9 booleans as given in the hand out: RegDst, Jump, Branch
      # MemRead, MemToReg, ALUOp1,ALUOp0,MemWrite,ALUSrc,RegWrite
      # (in this order)
      """
      O = opcode
      
      RegDist = ( not O[5] and not O[4] and not O[3] and not O[2] and not O[1] and not O[0] )
      Jump    = ( not O[5] and not O[4] and not O[3] and not O[2] and O[1] and not O[0] )
      Branch  = ( not O[5] and not O[4] and not O[3] and O[2] and not O[1] and not O[0] )
      MemRead = ( O[5] and not O[4] and not O[3] and not O[2] and O[1] and O[0] )
      Mem2Reg = ( O[5] and not O[4] and not O[3] and not O[2] and O[1] and O[0] )
      ALUOp1  = ( not O[5] and not O[4] and not O[3] and not O[2] and not O[1] and not O[0] )
      ALUOp0  = ( not O[5] and not O[4] and not O[3] and O[2] and not O[1] and not O[0] ) 
      MemWrt  = ( O[5] and not O[4] and O[3] and not O[2] and O[1] and O[0] )
      ALUSrc  = ( not O[5] and not O[4] and O[3] and not O[2] and not O[1] and not O[0] or
                  O[5] and not O[4] and not O[2] and O[1] and O[0] )
      RegWrt  = ( not O[5] and not O[4] and not O[2] and not O[1] and not O[0] or
                  O[5] and not O[4] and not O[3] and not O[2] and O[1] and O[0] )
      
      return RegDist, Jump, Branch, MemRead, Mem2Reg, ALUOp1, ALUOp0, MemWrt, ALUSrc, RegWrt

   
   def ALUControl(ALUOp1,ALUOp0,func):
      """ ALU Controler
      ###############
      # pre: ALUOp1 and 2 are booleans from the Control Unit,
      #   func is a BoolArray of length 6 that is bits 0..5 of the instruction
      # Returns a BoolArray of length 4 for the ALU as given in the hand outs
      """
      ans = BoolArray('0',4)
      
      ans[2] = ( not ALUOp1 and ALUOp0 or
                 ALUOp1 and not ALUOp0 and not func[0] and func[1] and not func[2]
                 and not func[3] and not func[4] and func[5] )
      
      ans[1] = ( not ALUOp1 or
                 not func[0] and not func[2] and not func[3] and not func[4] and func[5] )

      ans[0] = ( ALUOp1 and not ALUOp0 and func[0] and not func[1] and func[2] and not func[3]
                 and not func[4] and func[5] )

      return ans


   def ALU1(a,b,binvert,s1,s0,cin):
      """ 1-bit ALU
      ###########
      # pre: all inputs are Booleans
      # a and b are input bits, binvert is a bit to control the b inverter
      # cin is the carry in from the previous stage
      # s1 and s0 select one of the outpts from the mux
      #   FF-and, FT-or, TF-adder
      # see diagram for more details
      # returns r and cout the result bit and the carryout bit
      """
      A, B, BBar = BoolArray('0', 1), BoolArray('0', 1), BoolArray('0', 1)
      A[0], B[0], BBar[0] = a, b, not b

      Bfinal = CombinationalCircuits.mux2(B,BBar, binvert)

   
      MuxA, MuxB = BoolArray('0', 1), BoolArray('0', 1)
      MuxC, MuxD = BoolArray('0', 1), BoolArray('0', 1)
      
      MuxA[0] = A[0] and Bfinal[0]
      MuxB[0] = A[0] or Bfinal[0]
      MuxC[0], cout = CombinationalCircuits.Add1(A[0],Bfinal[0],cin)
      
      return CombinationalCircuits.mux4(MuxA,MuxB,MuxC,MuxD,s1,s0)[0], cout

      
   def ALU32(A,B,op):
      """
      # pre: A and B are BoolArrays of length 32
      #        op is a BoolArray of length 4 (from ALUControl unit)
      # see handouts for more details. FFFF-and, FFFT-or, FFTF-add,
      # FTTF-subt. The method is impemented as ripple ALU using a loop
      # calling ALU1() 32 times.  See handout diagram
      # returns result,zero where result is a BoolArray of length 32 and
      # represents the 'result' of the desired operation. zero is a boolean
      # and is True if the result is numerically zero
      """
      s0, s1, binv, cin = op[0], op[1], op[2], (not op[0] and op[1] and op[2] and not op[3])
      ans = BoolArray('0', 32)

      bools = []
      for i in range(32):
         ans[i], cin = CombinationalCircuits.ALU1(A[i],B[i],binv,s1,s0,cin)
         bools.append(not ans[i])
      return ans, all(bools)


   def Add1(a,b,cin):
      """ 1-bit Adder
      # pre: a,b,cin are booleans
      # post: ans, cout are bools
      """
      cout = (a and b  or  a and cin or
              b and cin)
      
      ans = (not a and not b and cin or
             not a and b and not cin or
             a and not b and not cin or
             a and b and cin)

      return ans, cout


   def Add(A,B):
      """32-bit Adder
      #############
      # pre: A and B are BoolArrays of length 32
      # return the sum of A and B as a BoolArray of length 32
      """
      ans, z = CombinationalCircuits.ALU32(A,B,BoolArray('2',4))
      return ans

   
   def Add4(Ain):
      """ Increment by 4
      ################
      # pre: Ain is a BoolArray of length 32
      # return a BoolArray whose integer value is the Ain + 4
      """
      ans, z = CombinationalCircuits.ALU32(Ain,BoolArray('4',32),BoolArray('2',4))
      return ans

