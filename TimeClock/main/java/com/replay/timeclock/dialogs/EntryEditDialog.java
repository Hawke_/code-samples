package com.replay.timeclock.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.replay.timeclock.R;
import com.replay.timeclock.TimeConverter;
import com.replay.timeclock.dbmanager.Entry;
import com.replay.timeclock.dbmanager.Job;

import java.util.Calendar;


public class EntryEditDialog extends DialogFragment {

    public interface EntryEditDialogListener {
        void edited(Parcelable p);
    }

    private Parcelable parcelable;
    private EntryEditDialogListener editDialogListener;

    public static EntryEditDialog newInstance(Parcelable p) {
        EntryEditDialog e = new EntryEditDialog();
        Bundle args = new Bundle();
        args.putParcelable("parcelable", p);
        e.setArguments(args);
        return e;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parcelable = getArguments().getParcelable("parcelable");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.entryedit_dialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);

        final DatePicker startDate = (DatePicker) view.findViewById(R.id.edit_dialog_start_date);
        final TimePicker startTime = (TimePicker) view.findViewById(R.id.edit_dialog_start_time);
        final DatePicker endDate = (DatePicker) view.findViewById(R.id.edit_dialog_end_date);
        final TimePicker endTime = (TimePicker) view.findViewById(R.id.edit_dialog_end_time);

        final Entry e = (Entry) parcelable;

        final TimeConverter timeConverter = new TimeConverter();
        final Calendar startCalendar = timeConverter.stringToDate(e.startTime);
        Calendar endCalendar = timeConverter.stringToDate(e.endTime);

        startDate.updateDate(startCalendar.get(Calendar.YEAR), startCalendar.get(Calendar.MONTH), startCalendar.get(Calendar.DAY_OF_MONTH));
        startTime.setHour(startCalendar.get(Calendar.HOUR_OF_DAY));
        startTime.setMinute(startCalendar.get(Calendar.MINUTE));

        endDate.updateDate(endCalendar.get(Calendar.YEAR), endCalendar.get(Calendar.MONTH), endCalendar.get(Calendar.DAY_OF_MONTH));
        endTime.setHour(endCalendar.get(Calendar.HOUR_OF_DAY));
        endTime.setMinute(endCalendar.get(Calendar.MINUTE));


        builder.setTitle("Edit Entry");
        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Calendar startCalendar = Calendar.getInstance();
                Calendar endCalendar = Calendar.getInstance();

                startCalendar.set(startDate.getYear(),startDate.getMonth(),startDate.getDayOfMonth(),startTime.getHour(),startTime.getMinute(), 0);
                endCalendar.set(endDate.getYear(),endDate.getMonth(),endDate.getDayOfMonth(),endTime.getHour(),endTime.getMinute(), 0);

                e.startTime = timeConverter.dateToString(startCalendar);
                e.endTime = timeConverter.dateToString(endCalendar);
                e.hours = timeConverter.differenceInDate(endCalendar,startCalendar);

                editDialogListener.edited(e);
                dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismiss();
            }
        });

        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.editDialogListener = (EntryEditDialogListener) activity;
        } catch (final ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnCompleteListener");
        }
    }
}
