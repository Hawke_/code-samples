package com.replay.timeclock.dbmanager;

import android.os.Parcel;
import android.os.Parcelable;


public class Job implements Parcelable {
    public int id;
    public String name;
    public float hours;
    public float wage;

    public Job() {

    }
    public Job(String name, float hours, float wage) {
        this.id = id;
        this.name = name;
        this.hours = hours;
        this.wage = wage;
    }


    protected Job(Parcel in) {
        id = in.readInt();
        name = in.readString();
        hours = in.readFloat();
        wage = in.readFloat();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeFloat(hours);
        dest.writeFloat(wage);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Job> CREATOR = new Parcelable.Creator<Job>() {
        @Override
        public Job createFromParcel(Parcel in) {
            return new Job(in);
        }

        @Override
        public Job[] newArray(int size) {
            return new Job[size];
        }
    };
}