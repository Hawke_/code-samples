package com.replay.timeclock.listview;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.replay.timeclock.R;

/**
 * Created by Nathan on 1/14/2017.
 */

public class EntryElement implements Item {
    private final String start, end, hours;
    private final int id;

    public EntryElement(String start, String end, Float hours, int id) {
        this.start = start;
        this.end = end;
        this.hours = String.valueOf(hours);
        this.id = id;
    }

    @Override
    public int getViewType() {
        return ListAdapter.RowType.ENTRY_ELEMENT.ordinal();
    }

    @Override
    public View getView(LayoutInflater inflater, View convertView) {
        View view;
        if (convertView == null) {
            view = inflater.inflate(R.layout.entry_element,null);
        } else {
            view = convertView;
        }
        TextView startText = (TextView) view.findViewById(R.id.entry_element_text_start);
        TextView endText = (TextView) view.findViewById(R.id.entry_element_text_end);
        TextView hoursText = (TextView) view.findViewById(R.id.entry_element_text_hours);

        startText.setText("Start: " + start);
        endText.setText("End " + end);
        hoursText.setText(hours + " hrs.");

        return view;
    }

    @Override
    public int getID(){
        return id;
    }
}
