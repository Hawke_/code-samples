package com.replay.timeclock.listview;

import android.content.Context;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.replay.timeclock.listview.Item;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nathan on 1/1/2017.
 */

public class ListAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private Context mContext;
    private ArrayList<Item> mItems;

    public enum RowType {
        ADD_ELEMENT,LIST_ELEMENT,ENTRY_ELEMENT
    }

    public ListAdapter(Context context, ArrayList<Item> items) {
        mInflater = LayoutInflater.from(context);
        mItems = items;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Item getItem(int i) {
        return mItems.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        return this.getItem(position).getView(mInflater,convertView);
    }

    @Override
    public int getViewTypeCount() {
        return RowType.values().length;
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).getViewType();
    }

    public void updateItems(ArrayList<Item> items) {
        mItems = items;
        super.notifyDataSetChanged();
    }

}
