//-----------------------------------------------------------------------
// config.h for "lin_slave_example.c" or "lin_master_example.c"
//-----------------------------------------------------------------------
                                                                                                                           
//----- I N C L U D E S
#include <avr/io.h>                 // Use AVR-GCC library

//----- D E C L A R A T I O N S
#define FOSC            8000        // in KHz
#define LIN_BAUDRATE    19200		// in bit/s
