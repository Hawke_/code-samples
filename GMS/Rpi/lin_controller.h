#include <time.h>
#include <wiringPi.h>
#include <pthread.h>
#include <stdint.h>

void init_controller(void);
void lin_transmit_byte(uint8_t byte);
uint8_t lin_receive_byte(void);
uint8_t lin_exchange(uint8_t id);
void init_controller(void);
