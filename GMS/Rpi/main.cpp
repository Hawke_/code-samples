#include "lin_controller.h"
#include "Sensor.h"
#include <iostream>
#include <string>
#include <stdint.h>

using namespace std;

void initialize(Sensor * list) {

	uint8_t response = 0xFF;
	uint8_t cid = 0x01;
	int iter = 0;
	char rstr[30];
	char rstr2[30];

	while (response != 0x00) {
		response = lin_exchange(cid);
		
		sprintf(rstr, "%X", (cid));
		sprintf(rstr2, "%X", response);
		cout << "ID: " << rstr << " | Response: " << rstr2 << "\n";

		if ((response != 0xFF) && (response != 0x00)) {
			list[iter].setID(cid);
			cid = (++cid) & 0xFF;
			iter++;
		}
		delay(1000);
	}
	cout << "End Initialize\n";
}

int main(void) {
	/*
	init_controller();
	uint8_t id; 
	uint8_t response;
	char rstr[30];

	while (1) {
		cin.clear();
		cin >> id;
		cout << "ID: " << id << "\n";
		response = lin_exchange(id);
		sprintf(rstr, "%X", response);
		cout << "Response | " << rstr << "\n";
		
	}
	*/
	
	
	Sensor id_list[64] = { Sensor(0x00, 0) };
	init_controller();
	initialize(id_list);
	
	uint8_t response;
	char rstr[30];
	int temp;

	while (1) {
		temp = 0;
		while (id_list[temp].getID() != 0x00) {
			response = lin_exchange(id_list[temp].getID());
			if (response == 0x00 || response == 0xFF) continue;

			sprintf(rstr, "%X", id_list[temp].getID());
			switch (response) {
				case 0xAA:
					id_list[temp].setStatus(DETECT);
					cout << rstr << " | Grain Detected\n";
					break;
				case 0x55:
					id_list[temp].setStatus(NODETECT);
					cout << rstr << " | No Grain Detected\n";
					break;
				default:
					cout << rstr << " | Error\n";
					continue;
					break;
			}
			++temp;
			delay(500);
		}
	}

	
	return 0;

	
}